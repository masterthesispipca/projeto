(Final - Entregue)
# README #

Participantes do projeto:
Márcio Barth;
Maurício de Oliveira Saraiva

O projeto está dividido em 2 partes:

1. Casos de uso;
2. Projeto (códigos fonte do jogo).

Parte 1:
Arquivo casos_de_uso.doc
Contem a descrição dos casos de uso de cada fase;
O arquivo atualizado fica na pasta /casos_de_uso

Parte 2:
Arquivos fonte do jogo. São 8 fases divididas em 8 pastas:
cursar_cadeiras
defesa
entrevista
inscricao
matricula
orientador
proficiencia
proposta

Também faz parte do sistema:
inicial - tela inicial onde se escolhe o personagem do jogo
mapa - tela principal do jogo (mapa)
final - tela de encerramento

Obs.: o projeto foi desenvolvido em Visual Basic 6. Para isso, necessita possuir a IDE do VB6 para windows 7 ou inferior.