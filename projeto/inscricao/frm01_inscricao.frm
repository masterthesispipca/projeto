VERSION 5.00
Begin VB.Form frm01_inscricao 
   BackColor       =   &H80000005&
   Caption         =   "Fase 1"
   ClientHeight    =   6090
   ClientLeft      =   3030
   ClientTop       =   1830
   ClientWidth     =   14550
   LinkTopic       =   "Form1"
   ScaleHeight     =   6090
   ScaleWidth      =   14550
   Begin VB.CommandButton Command1 
      Caption         =   "Atalho"
      Height          =   375
      Left            =   13560
      TabIndex        =   2
      Top             =   120
      Width           =   855
   End
   Begin VB.Label lbl_descricao 
      BackColor       =   &H80000005&
      Caption         =   "Encontre os documentos (original e c�pia) para realizar a inscri��o no mestrado em Computa��o Aplicada da Unisinos."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   120
      TabIndex        =   1
      Top             =   840
      Width           =   14175
   End
   Begin VB.Image doc 
      Height          =   1575
      Index           =   9
      Left            =   8760
      Picture         =   "frm01_inscricao.frx":0000
      Stretch         =   -1  'True
      Top             =   4080
      Width           =   2535
   End
   Begin VB.Image doc 
      Height          =   1575
      Index           =   8
      Left            =   6000
      Picture         =   "frm01_inscricao.frx":4CC2
      Stretch         =   -1  'True
      Top             =   4080
      Width           =   2535
   End
   Begin VB.Image doc 
      Height          =   1575
      Index           =   7
      Left            =   3240
      Picture         =   "frm01_inscricao.frx":9984
      Stretch         =   -1  'True
      Top             =   4080
      Width           =   2535
   End
   Begin VB.Image doc 
      Height          =   1575
      Index           =   6
      Left            =   480
      Picture         =   "frm01_inscricao.frx":E646
      Stretch         =   -1  'True
      Top             =   4080
      Width           =   2535
   End
   Begin VB.Image doc 
      Height          =   1575
      Index           =   5
      Left            =   11520
      Picture         =   "frm01_inscricao.frx":13308
      Stretch         =   -1  'True
      Top             =   2280
      Width           =   2535
   End
   Begin VB.Image doc 
      Height          =   1575
      Index           =   4
      Left            =   8760
      Picture         =   "frm01_inscricao.frx":17FCA
      Stretch         =   -1  'True
      Top             =   2280
      Width           =   2535
   End
   Begin VB.Image doc 
      Height          =   1575
      Index           =   3
      Left            =   6000
      Picture         =   "frm01_inscricao.frx":1CC8C
      Stretch         =   -1  'True
      Top             =   2280
      Width           =   2535
   End
   Begin VB.Image doc 
      Height          =   1575
      Index           =   10
      Left            =   11520
      Picture         =   "frm01_inscricao.frx":2194E
      Stretch         =   -1  'True
      Top             =   4080
      Width           =   2535
   End
   Begin VB.Image doc 
      Height          =   1575
      Index           =   2
      Left            =   3240
      Picture         =   "frm01_inscricao.frx":26610
      Stretch         =   -1  'True
      Top             =   2280
      Width           =   2535
   End
   Begin VB.Image doc 
      Height          =   1575
      Index           =   1
      Left            =   480
      Picture         =   "frm01_inscricao.frx":2B2D2
      Stretch         =   -1  'True
      Top             =   2280
      Width           =   2535
   End
   Begin VB.Label lbl_titulo 
      BackColor       =   &H80000005&
      Caption         =   "Inscri��o"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   26.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   735
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   14175
   End
   Begin VB.Shape Shape1 
      Height          =   3855
      Left            =   240
      Top             =   2040
      Width           =   14055
   End
End
Attribute VB_Name = "frm01_inscricao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Sub Sleep Lib "kernel32.dll" (ByVal dwMilliseconds As Long)

Const numCartas = 10
Dim carta(10) As String * 1
Dim posicao(10, 2) As Long
Dim carta1 As Integer
Dim carta2 As Integer

Private Sub Command1_Click()
    MsgBox "Fase " & Fase & " conclu�da!", vbInformation
    MudaFase Fase
End Sub

Private Sub doc_Click(Index As Integer)
  If carta(Index) = "S" Then
    Exit Sub
  End If

  If carta1 = 0 Then
    carta1 = Index
    mostraCarta carta1
  ElseIf carta2 = 0 Then
    carta2 = Index
    mostraCarta carta2
  End If
  
  DoEvents
  If carta1 <> 0 And carta2 <> 0 Then
     verificaPares carta1, carta2
  End If
End Sub

Private Sub Form_Load()
Dim Num As Integer

  For Num = 1 To numCartas
    carta(Num) = "N"
    posicao(Num, 1) = Me.doc(Num).Left
    posicao(Num, 2) = Me.doc(Num).Top
  Next

  carta1 = 0
  carta2 = 0
  
  Me.MousePointer = 11
  Sleep 1000 'espera 2s para mostrar movimento no grafo
  Me.MousePointer = 1
  
  DoEvents
  embaralhaCartas
End Sub

Private Sub verificaPares(ByVal c1 As Integer, ByVal c2 As Integer)

  If Abs(c2 - c1) = 5 Then
    carta(c1) = "S"
    carta(c2) = "S"
    Me.doc(c1).Picture = LoadPicture(App.Path & "\inscricao\imagem\doc" & c1 & ".jpg")
    Me.doc(c2).Picture = LoadPicture(App.Path & "\inscricao\imagem\doc" & c2 & ".jpg")
  Else
    'MsgBox "Errou"
    Me.MousePointer = 11
    Sleep 1000 'espera 2s para mostrar movimento no grafo
    Me.MousePointer = 1

    Me.doc(c1).Picture = LoadPicture(App.Path & "\inscricao\imagem\charada.jpg")
    Me.doc(c2).Picture = LoadPicture(App.Path & "\inscricao\imagem\charada.jpg")
  End If

  If Not verificaFim Then
    carta1 = 0
    carta2 = 0
    embaralhaCartas
  Else
    MsgBox "Fase " & Fase & " conclu�da!", vbInformation
    MudaFase 1
  End If
End Sub

Private Function verificaFim() As Boolean
Dim Pos As Integer
  
  verificaFim = True
  For Pos = 1 To numCartas
    If carta(Pos) = "N" Then
      verificaFim = False
      Exit For
    End If
  Next
End Function

Private Sub embaralhaCartas()
Dim vetCartas1(10) As Integer
Dim vetCartas2(10) As Integer
Dim Esquerda As Long
Dim topo As Long
Dim proximo As Integer
Dim atual As Integer
Dim salva As Integer
Dim ordem As Integer
Dim Pos As Integer

  ordem = 0
  For Num = 1 To 10
    If carta(Num) = "N" Then
      ordem = ordem + 1
      vetCartas1(ordem) = Num
      vetCartas2(ordem) = Num
    End If
  Next

  For Pos = 1 To ordem
    Randomize
    atual = Int(((ordem) * Rnd()) + 1)
    Randomize
    proximo = Int(((ordem) * Rnd()) + 1)
     
    salva = vetCartas2(atual)
    vetCartas2(atual) = vetCartas2(proximo)
    vetCartas2(proximo) = salva
  Next

  For Pos = 1 To ordem
    Esquerda = Me.doc(vetCartas1(Pos)).Left
    topo = Me.doc(vetCartas1(Pos)).Top
    Me.doc(vetCartas1(Pos)).Left = Me.doc(vetCartas2(Pos)).Left
    Me.doc(vetCartas1(Pos)).Top = Me.doc(vetCartas2(Pos)).Top
    Me.doc(vetCartas2(Pos)).Left = Esquerda
    Me.doc(vetCartas2(Pos)).Top = topo
  Next
End Sub

Private Sub mostraCarta(ByVal c1 As Integer)
  Me.doc(c1).Picture = LoadPicture(App.Path & "\inscricao\imagem\doc" & c1 & ".jpg")
End Sub
