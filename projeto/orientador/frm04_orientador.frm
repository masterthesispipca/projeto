VERSION 5.00
Begin VB.Form frm04_orientador 
   BackColor       =   &H80000005&
   Caption         =   "Fase 3"
   ClientHeight    =   6495
   ClientLeft      =   1755
   ClientTop       =   1905
   ClientWidth     =   17055
   LinkTopic       =   "Form1"
   ScaleHeight     =   6495
   ScaleWidth      =   17055
   Begin VB.CommandButton Command1 
      Caption         =   "Atalho"
      Height          =   375
      Left            =   16080
      TabIndex        =   4
      Top             =   120
      Width           =   855
   End
   Begin VB.Timer Tempo 
      Left            =   14520
      Top             =   120
   End
   Begin VB.Label lbl_orientador 
      Alignment       =   2  'Center
      BackColor       =   &H80000005&
      Caption         =   "Linha de pesquisa"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   20.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   1
      Left            =   120
      TabIndex        =   3
      Top             =   5760
      Width           =   16935
   End
   Begin VB.Label lbl_orientador 
      Alignment       =   2  'Center
      BackColor       =   &H80000005&
      Caption         =   "Orientador"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   20.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   0
      Left            =   120
      TabIndex        =   2
      Top             =   5280
      Width           =   16935
   End
   Begin VB.Image img_seta 
      Height          =   1215
      Left            =   240
      Picture         =   "frm04_orientador.frx":0000
      Stretch         =   -1  'True
      Top             =   3720
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   5
      Left            =   6240
      Picture         =   "frm04_orientador.frx":227C
      Stretch         =   -1  'True
      Tag             =   "Jose Vicente Canto dos Santos"
      ToolTipText     =   "Pesquisa Operacional"
      Top             =   2400
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   3
      Left            =   3840
      Picture         =   "frm04_orientador.frx":728C
      Stretch         =   -1  'True
      Tag             =   "Joao Francisco Valiati"
      ToolTipText     =   "Intelig�ncia Artificial"
      Top             =   2400
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   13
      Left            =   15840
      Picture         =   "frm04_orientador.frx":B18B
      Stretch         =   -1  'True
      Tag             =   "Sandro Jose Rigo"
      ToolTipText     =   "Intelig�ncia Artificial"
      Top             =   2400
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   12
      Left            =   14640
      Picture         =   "frm04_orientador.frx":C8A7
      Stretch         =   -1  'True
      Tag             =   "Rodrigo da Rosa Righi"
      ToolTipText     =   "Redes de Computadores e Sistemas Distribu�dos"
      Top             =   2400
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   11
      Left            =   13440
      Picture         =   "frm04_orientador.frx":11926
      Stretch         =   -1  'True
      Tag             =   "Patricia Augustin Jaques Maillard"
      ToolTipText     =   "Intelig�ncia Artificial"
      Top             =   2400
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   10
      Left            =   12240
      Picture         =   "frm04_orientador.frx":16923
      Stretch         =   -1  'True
      Tag             =   "Marta Becker Villamil"
      ToolTipText     =   "Computa��o Gr�fica e Processamento de Imagens"
      Top             =   2400
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   9
      Left            =   11040
      Picture         =   "frm04_orientador.frx":1B855
      Stretch         =   -1  'True
      Tag             =   "Luiz Paulo Luna de Oliveira"
      ToolTipText     =   "Pesquisa Operacional, Computa��o Gr�fica e Processamento de Imagens"
      Top             =   2400
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   7
      Left            =   8640
      Picture         =   "frm04_orientador.frx":206ED
      Stretch         =   -1  'True
      Tag             =   "Leonardo Dagnino Chiwiacowsky"
      ToolTipText     =   "Pesquisa Operacional"
      Top             =   2400
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   6
      Left            =   7440
      Picture         =   "frm04_orientador.frx":250C5
      Stretch         =   -1  'True
      Tag             =   "Kleinner Silva Farias de Oliveira"
      ToolTipText     =   "Engenharia de Software e Linguagens de Programa��o"
      Top             =   2400
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   2
      Left            =   2640
      Picture         =   "frm04_orientador.frx":28A4B
      Stretch         =   -1  'True
      Tag             =   "Joao Carlos Gluz"
      ToolTipText     =   "Engenharia de Software e Linguagens de Programa��o"
      Top             =   2400
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   8
      Left            =   9840
      Picture         =   "frm04_orientador.frx":2D7FF
      Stretch         =   -1  'True
      Tag             =   "Luiz Gonzaga da Silveira Junior"
      ToolTipText     =   "Computa��o Gr�fica e Processamento de Imagens"
      Top             =   2400
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   1
      Left            =   1440
      Picture         =   "frm04_orientador.frx":3232D
      Stretch         =   -1  'True
      Tag             =   "Cristiano Andre da Costa"
      ToolTipText     =   "Redes de Computadores e Sistemas Distribu�dos"
      Top             =   2400
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   4
      Left            =   5040
      Picture         =   "frm04_orientador.frx":35AEF
      Stretch         =   -1  'True
      Tag             =   "Jorge Luis Victoria Barbosa"
      ToolTipText     =   "Engenharia de Software e Linguagens de Programa��o"
      Top             =   2400
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   0
      Left            =   240
      Picture         =   "frm04_orientador.frx":3A9C7
      Stretch         =   -1  'True
      Tag             =   "Arthur Torgo Gomez"
      ToolTipText     =   "Pesquisa Operacional"
      Top             =   2400
      Width           =   975
   End
   Begin VB.Label lbl_descricao 
      BackColor       =   &H80000005&
      Caption         =   $"frm04_orientador.frx":3FA6B
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   120
      TabIndex        =   1
      Top             =   960
      Width           =   16695
   End
   Begin VB.Label lbl_titulo 
      BackColor       =   &H80000005&
      Caption         =   "Escolha do orientador"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   26.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   735
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   14175
   End
End
Attribute VB_Name = "frm04_orientador"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim contador As Integer
Dim posicao As Integer
Dim sentido As String

Private Sub sorteia_inicio()
  Randomize
  posicao = Int(((13) * Rnd()) + 1)
  Me.img_seta.Left = Me.img_orientador(posicao).Left
  Me.lbl_orientador(0).Caption = Me.img_orientador(posicao).Tag
  Me.lbl_orientador(1).Caption = Me.img_orientador(posicao).ToolTipText
End Sub

Private Sub Command1_Click()
  MsgBox "Fase " & Fase & " conclu�da!", vbInformation
  MudaFase Fase
End Sub

Private Sub Form_Load()
  sorteia_inicio
  Me.Tempo.Interval = 0
End Sub

Private Sub img_orientador_Click(Index As Integer)
  If Me.lbl_orientador(0).Caption = img_orientador(Index).Tag Then
    MsgBox "Fase " & Fase & " conclu�da!", vbInformation
    MudaFase Fase
  Else
    MsgBox "Esse n�o � o orientador selecionado!", vbCritical
  End If
End Sub

Private Sub img_seta_Click()
  sorteia_inicio
  contador = 0
  sentido = "d"
  Me.Tempo.Interval = 5
  Me.lbl_orientador(0).Caption = ""
  Me.lbl_orientador(1).Caption = ""
  DoEvents
End Sub

Private Sub Tempo_Timer()
Dim posicao_atual As Long

  If posicao = 0 Then
    sentido = "d"
  ElseIf posicao = 13 Then
    sentido = "e"
  End If
  
  posicao_atual = posicao
  Me.img_seta.Left = Me.img_orientador(posicao_atual).Left
  Me.lbl_orientador(0).Caption = Me.img_orientador(posicao_atual).Tag
  Me.lbl_orientador(1).Caption = Me.img_orientador(posicao_atual).ToolTipText
  
  If sentido = "d" Then
    posicao = posicao + 1
  Else
    posicao = posicao - 1
  End If
  
  contador = contador + 1
  If contador Mod 5 = 0 And contador <= 65 Then
    Me.Tempo.Interval = Me.Tempo.Interval + 5
  ElseIf contador Mod 5 = 0 And contador > 65 And contador <= 90 Then
    Me.Tempo.Interval = Me.Tempo.Interval + 50
  ElseIf contador Mod 5 = 0 And contador >= 90 And contador < 99 Then
    Me.Tempo.Interval = Me.Tempo.Interval + 100
  ElseIf contador Mod 5 = 0 And contador >= 99 Then
    Me.Tempo.Interval = Me.Tempo.Interval + 500
  End If
  
  If contador > 101 Then
    Me.Tempo.Interval = 0
    Me.lbl_orientador(0).Caption = Me.img_orientador(posicao_atual).Tag
    Me.lbl_orientador(1).Caption = Me.img_orientador(posicao_atual).ToolTipText
  End If
  
  DoEvents
End Sub
