VERSION 5.00
Begin VB.Form frm04_orientador 
   BackColor       =   &H80000005&
   Caption         =   "orientador para mestrado"
   ClientHeight    =   6780
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   17235
   LinkTopic       =   "Form1"
   ScaleHeight     =   6780
   ScaleWidth      =   17235
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Tempo 
      Left            =   14520
      Top             =   120
   End
   Begin VB.Line Line1 
      Index           =   12
      X1              =   10920
      X2              =   10920
      Y1              =   2040
      Y2              =   3480
   End
   Begin VB.Line Line1 
      Index           =   11
      X1              =   12120
      X2              =   12120
      Y1              =   2040
      Y2              =   3480
   End
   Begin VB.Line Line1 
      Index           =   10
      X1              =   13320
      X2              =   13320
      Y1              =   2040
      Y2              =   3480
   End
   Begin VB.Line Line1 
      Index           =   9
      X1              =   14520
      X2              =   14520
      Y1              =   2040
      Y2              =   3480
   End
   Begin VB.Line Line1 
      Index           =   8
      X1              =   15720
      X2              =   15720
      Y1              =   2040
      Y2              =   3480
   End
   Begin VB.Line Line1 
      Index           =   7
      X1              =   9720
      X2              =   9720
      Y1              =   2040
      Y2              =   3480
   End
   Begin VB.Line Line1 
      Index           =   6
      X1              =   8520
      X2              =   8520
      Y1              =   2040
      Y2              =   3480
   End
   Begin VB.Line Line1 
      Index           =   5
      X1              =   7320
      X2              =   7320
      Y1              =   2040
      Y2              =   3480
   End
   Begin VB.Line Line1 
      Index           =   4
      X1              =   6120
      X2              =   6120
      Y1              =   2040
      Y2              =   3480
   End
   Begin VB.Line Line1 
      Index           =   3
      X1              =   4920
      X2              =   4920
      Y1              =   2040
      Y2              =   3480
   End
   Begin VB.Line Line1 
      Index           =   2
      X1              =   3720
      X2              =   3720
      Y1              =   2040
      Y2              =   3480
   End
   Begin VB.Line Line1 
      Index           =   1
      X1              =   2520
      X2              =   2520
      Y1              =   2040
      Y2              =   3480
   End
   Begin VB.Line Line1 
      Index           =   0
      X1              =   1320
      X2              =   1320
      Y1              =   2040
      Y2              =   3480
   End
   Begin VB.Label lbl_orientador 
      Caption         =   "Linha de pesquisa:"
      Height          =   375
      Index           =   1
      Left            =   8400
      TabIndex        =   3
      Top             =   5880
      Width           =   4455
   End
   Begin VB.Label lbl_orientador 
      Caption         =   "Orientador:"
      Height          =   375
      Index           =   0
      Left            =   8400
      TabIndex        =   2
      Top             =   5400
      Width           =   4455
   End
   Begin VB.Shape Shape1 
      Height          =   1455
      Left            =   120
      Top             =   2040
      Width           =   16815
   End
   Begin VB.Image img_seta 
      Height          =   975
      Left            =   8760
      Picture         =   "frm04_orientador.frx":0000
      Stretch         =   -1  'True
      Top             =   3600
      Width           =   735
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   13
      Left            =   15840
      Picture         =   "frm04_orientador.frx":0BCB
      Stretch         =   -1  'True
      Tag             =   "Jose Vicente Canto dos Santos"
      ToolTipText     =   "Pesquisa Operacional"
      Top             =   2160
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   12
      Left            =   14640
      Picture         =   "frm04_orientador.frx":5BDB
      Stretch         =   -1  'True
      Tag             =   "Joao Francisco Valiati"
      ToolTipText     =   "Intelig�ncia Artificial"
      Top             =   2160
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   11
      Left            =   13440
      Picture         =   "frm04_orientador.frx":9ADA
      Stretch         =   -1  'True
      Tag             =   "Sandro Jose Rigo"
      ToolTipText     =   "Intelig�ncia Artificial"
      Top             =   2160
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   10
      Left            =   12240
      Picture         =   "frm04_orientador.frx":B1F6
      Stretch         =   -1  'True
      Tag             =   "Rodrigo da Rosa Righi"
      ToolTipText     =   "Redes de Computadores e Sistemas Distribu�dos"
      Top             =   2160
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   9
      Left            =   11040
      Picture         =   "frm04_orientador.frx":10275
      Stretch         =   -1  'True
      Tag             =   "Patricia Augustin Jaques Maillard"
      ToolTipText     =   "Intelig�ncia Artificial"
      Top             =   2160
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   8
      Left            =   9840
      Picture         =   "frm04_orientador.frx":15272
      Stretch         =   -1  'True
      Tag             =   "Marta Becker Villamil"
      ToolTipText     =   "Computa��o Gr�fica e Processamento de Imagens"
      Top             =   2160
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   7
      Left            =   8640
      Picture         =   "frm04_orientador.frx":1A1A4
      Stretch         =   -1  'True
      Tag             =   "Luiz Paulo Luna de Oliveira"
      ToolTipText     =   "Pesquisa Operacional, Computa��o Gr�fica e Processamento de Imagens"
      Top             =   2160
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   6
      Left            =   7440
      Picture         =   "frm04_orientador.frx":1F03C
      Stretch         =   -1  'True
      Tag             =   "Leonardo Dagnino Chiwiacowsky"
      ToolTipText     =   "Pesquisa Operacional"
      Top             =   2160
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   5
      Left            =   6240
      Picture         =   "frm04_orientador.frx":23A14
      Stretch         =   -1  'True
      Tag             =   "Kleinner Silva Farias de Oliveira"
      ToolTipText     =   "Engenharia de Software e Linguagens de Programa��o"
      Top             =   2160
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   4
      Left            =   5040
      Picture         =   "frm04_orientador.frx":2739A
      Stretch         =   -1  'True
      Tag             =   "Joao Carlos Gluz"
      ToolTipText     =   "Engenharia de Software e Linguagens de Programa��o"
      Top             =   2160
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   3
      Left            =   3840
      Picture         =   "frm04_orientador.frx":2C14E
      Stretch         =   -1  'True
      Tag             =   "Luiz Gonzaga da Silveira Junior"
      ToolTipText     =   "Computa��o Gr�fica e Processamento de Imagens"
      Top             =   2160
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   2
      Left            =   2640
      Picture         =   "frm04_orientador.frx":30C7C
      Stretch         =   -1  'True
      Tag             =   "Cristiano Andre da Costa"
      ToolTipText     =   "Redes de Computadores e Sistemas Distribu�dos"
      Top             =   2160
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   1
      Left            =   1440
      Picture         =   "frm04_orientador.frx":3443E
      Stretch         =   -1  'True
      Tag             =   "Jorge Luis Victoria Barbosa"
      ToolTipText     =   "Engenharia de Software e Linguagens de Programa��o"
      Top             =   2160
      Width           =   975
   End
   Begin VB.Image img_orientador 
      Height          =   1215
      Index           =   0
      Left            =   240
      Picture         =   "frm04_orientador.frx":39316
      Stretch         =   -1  'True
      Tag             =   "Arthur Torgo Gomez"
      ToolTipText     =   "Pesquisa Operacional"
      Top             =   2160
      Width           =   975
   End
   Begin VB.Label lbl_descricao 
      BackColor       =   &H80000005&
      Caption         =   "Escolher o orientador � necess�rio para in�cio do mestrado"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   120
      TabIndex        =   1
      Top             =   960
      Width           =   14895
   End
   Begin VB.Label lbl_titulo 
      BackColor       =   &H80000005&
      Caption         =   "Escolha do orientador"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   26.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   735
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   14175
   End
End
Attribute VB_Name = "frm04_orientador"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim orientador(13, 1) As String
Dim contador As Integer

Private Sub Form_Load()
Dim pos As Integer

  For pos = 0 To 13
    orientador(pos, 0) = Me.img_orientador(pos).Tag 'nome
    orientador(pos, 1) = Me.img_orientador(pos).ToolTipText 'linha de pesquisa
  Next

  Me.Tempo.Interval = 0
End Sub

Private Sub img_seta_Click()
  Me.Tempo.Interval = 5
  contador = 0
  Me.lbl_orientador(0).Caption = "Orientador: "
  Me.lbl_orientador(1).Caption = "Linha de pesquisa: "
End Sub

Private Sub Tempo_Timer()
Dim salva(1, 1) As String
Dim pos As Integer

  salva(0, 0) = orientador(13, 0)
  salva(0, 1) = orientador(13, 1)
  For pos = 13 To 1 Step -1
    orientador(pos, 0) = orientador(pos - 1, 0)
    orientador(pos, 1) = orientador(pos - 1, 1)
  Next
  orientador(0, 0) = salva(0, 0)
  orientador(0, 1) = salva(0, 1)
  
  For pos = 0 To 13
    Me.img_orientador(pos).Picture = LoadPicture(App.Path & "\orientador\imagem\" & orientador(pos, 0) & ".jpg")
    Me.img_orientador(pos).Tag = orientador(pos, 0)
    Me.img_orientador(pos).ToolTipText = orientador(pos, 1)
  Next
  
  contador = contador + 1
  If contador Mod 5 = 0 And contador <= 65 Then
    Me.Tempo.Interval = Me.Tempo.Interval + 5
  ElseIf contador Mod 5 = 0 And contador > 65 And contador <= 90 Then
    Me.Tempo.Interval = Me.Tempo.Interval + 50
  ElseIf contador Mod 5 = 0 And contador >= 90 And contador < 99 Then
    Me.Tempo.Interval = Me.Tempo.Interval + 100
  ElseIf contador Mod 5 = 0 And contador >= 99 Then
    Me.Tempo.Interval = Me.Tempo.Interval + 500
  End If
  
  If contador > 101 Then
    Me.Tempo.Interval = 0
    Me.lbl_orientador(0).Caption = "Orientador: " & orientador(7, 0)
    Me.lbl_orientador(1).Caption = "Linha de pesquisa: " & orientador(7, 1)
  End If
  
  DoEvents
End Sub
