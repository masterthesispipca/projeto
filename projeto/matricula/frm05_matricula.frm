VERSION 5.00
Begin VB.Form frm05_matricula 
   BackColor       =   &H80000005&
   Caption         =   "Fase 4"
   ClientHeight    =   5205
   ClientLeft      =   2775
   ClientTop       =   1980
   ClientWidth     =   15030
   LinkTopic       =   "Form1"
   ScaleHeight     =   5205
   ScaleWidth      =   15030
   Begin VB.CommandButton Command1 
      Caption         =   "Atalho"
      Height          =   375
      Left            =   14040
      TabIndex        =   40
      Top             =   120
      Width           =   855
   End
   Begin VB.Frame frame_questao 
      BackColor       =   &H80000005&
      Height          =   2535
      Index           =   3
      Left            =   480
      TabIndex        =   6
      Top             =   4320
      Width           =   14535
      Begin VB.Label Tonalidade 
         Alignment       =   2  'Center
         BackColor       =   &H00400000&
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   27.75
            Charset         =   0
            Weight          =   900
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Index           =   12
         Left            =   11040
         TabIndex        =   39
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label Tonalidade 
         Alignment       =   2  'Center
         BackColor       =   &H00800000&
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   27.75
            Charset         =   0
            Weight          =   900
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Index           =   11
         Left            =   10080
         TabIndex        =   38
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label Tonalidade 
         Alignment       =   2  'Center
         BackColor       =   &H00C00000&
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   27.75
            Charset         =   0
            Weight          =   900
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Index           =   10
         Left            =   9120
         TabIndex        =   37
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label Tonalidade 
         Alignment       =   2  'Center
         BackColor       =   &H00FF0000&
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   27.75
            Charset         =   0
            Weight          =   900
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Index           =   9
         Left            =   8160
         TabIndex        =   36
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label Tonalidade 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   27.75
            Charset         =   0
            Weight          =   900
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Index           =   8
         Left            =   7200
         TabIndex        =   35
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label Tonalidade 
         Alignment       =   2  'Center
         BackColor       =   &H00FFC0C0&
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   27.75
            Charset         =   0
            Weight          =   900
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Index           =   7
         Left            =   6240
         TabIndex        =   34
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label Tonalidade 
         Alignment       =   2  'Center
         BackColor       =   &H00404000&
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   27.75
            Charset         =   0
            Weight          =   900
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Index           =   6
         Left            =   5280
         TabIndex        =   33
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label Tonalidade 
         Alignment       =   2  'Center
         BackColor       =   &H00808000&
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   27.75
            Charset         =   0
            Weight          =   900
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Index           =   5
         Left            =   4320
         TabIndex        =   32
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label Tonalidade 
         Alignment       =   2  'Center
         BackColor       =   &H00C0C000&
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   27.75
            Charset         =   0
            Weight          =   900
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Index           =   4
         Left            =   3360
         TabIndex        =   31
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label Tonalidade 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF00&
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   27.75
            Charset         =   0
            Weight          =   900
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Index           =   3
         Left            =   2400
         TabIndex        =   30
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label Tonalidade 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   27.75
            Charset         =   0
            Weight          =   900
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Index           =   2
         Left            =   1440
         TabIndex        =   29
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label Tonalidade 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   27.75
            Charset         =   0
            Weight          =   900
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Index           =   1
         Left            =   480
         TabIndex        =   28
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label lbl_q03 
         BackColor       =   &H80000005&
         Caption         =   "3. Clique na ordem correta dos tons (do verde para o azul)"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Index           =   1
         Left            =   240
         TabIndex        =   7
         Top             =   240
         Width           =   14055
      End
   End
   Begin VB.Frame frame_questao 
      BackColor       =   &H80000005&
      Height          =   2535
      Index           =   2
      Left            =   360
      TabIndex        =   4
      Top             =   3720
      Width           =   14535
      Begin VB.Label MenorQuadrado 
         BackColor       =   &H0000C000&
         Height          =   525
         Index           =   9
         Left            =   13320
         TabIndex        =   27
         Top             =   1320
         Width           =   525
      End
      Begin VB.Label MenorQuadrado 
         BackColor       =   &H0000C000&
         Height          =   1125
         Index           =   8
         Left            =   11640
         TabIndex        =   26
         Top             =   1320
         Width           =   1125
      End
      Begin VB.Label MenorQuadrado 
         BackColor       =   &H0000C000&
         Height          =   885
         Index           =   7
         Left            =   10080
         TabIndex        =   25
         Top             =   720
         Width           =   885
      End
      Begin VB.Label MenorQuadrado 
         BackColor       =   &H0000C000&
         Height          =   645
         Index           =   6
         Left            =   8760
         TabIndex        =   24
         Top             =   1200
         Width           =   645
      End
      Begin VB.Label MenorQuadrado 
         BackColor       =   &H0000C000&
         Height          =   765
         Index           =   5
         Left            =   7440
         TabIndex        =   23
         Top             =   600
         Width           =   765
      End
      Begin VB.Label MenorQuadrado 
         BackColor       =   &H0000C000&
         Height          =   405
         Index           =   4
         Left            =   6000
         TabIndex        =   22
         Top             =   1440
         Width           =   405
      End
      Begin VB.Label MenorQuadrado 
         BackColor       =   &H0000C000&
         Height          =   1005
         Index           =   3
         Left            =   4680
         TabIndex        =   21
         Top             =   960
         Width           =   1005
      End
      Begin VB.Label MenorQuadrado 
         BackColor       =   &H0000C000&
         Height          =   165
         Index           =   2
         Left            =   3480
         TabIndex        =   20
         Top             =   1680
         Width           =   185
      End
      Begin VB.Label MenorQuadrado 
         BackColor       =   &H0000C000&
         Height          =   285
         Index           =   1
         Left            =   2160
         TabIndex        =   19
         Top             =   840
         Width           =   305
      End
      Begin VB.Label MenorQuadrado 
         BackColor       =   &H0000C000&
         Height          =   255
         Index           =   10
         Left            =   840
         TabIndex        =   18
         Top             =   1200
         Width           =   275
      End
      Begin VB.Label lbl_q02 
         BackColor       =   &H80000005&
         Caption         =   "2. Clique no menor quadrado"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   0
         Left            =   240
         TabIndex        =   5
         Top             =   240
         Width           =   14055
      End
   End
   Begin VB.Frame frame_questao 
      BackColor       =   &H80000005&
      Height          =   2535
      Index           =   1
      Left            =   240
      TabIndex        =   2
      Top             =   3120
      Width           =   14535
      Begin VB.Label Quadrado 
         BackColor       =   &H8000000D&
         Height          =   375
         Index           =   6
         Left            =   12240
         TabIndex        =   17
         Top             =   960
         Width           =   390
      End
      Begin VB.Label Quadrado 
         BackColor       =   &H8000000D&
         Height          =   855
         Index           =   5
         Left            =   10080
         TabIndex        =   16
         Top             =   600
         Width           =   975
      End
      Begin VB.Label Quadrado 
         BackColor       =   &H8000000D&
         Height          =   1215
         Index           =   4
         Left            =   8160
         TabIndex        =   15
         Top             =   840
         Width           =   1335
      End
      Begin VB.Label Quadrado 
         BackColor       =   &H8000000D&
         Height          =   615
         Index           =   3
         Left            =   6360
         TabIndex        =   14
         Top             =   1080
         Width           =   735
      End
      Begin VB.Label Quadrado 
         BackColor       =   &H8000000D&
         Height          =   735
         Index           =   2
         Left            =   4440
         TabIndex        =   13
         Top             =   1320
         Width           =   615
      End
      Begin VB.Label Quadrado 
         BackColor       =   &H8000000D&
         Height          =   495
         Index           =   1
         Left            =   2520
         TabIndex        =   12
         Top             =   1320
         Width           =   615
      End
      Begin VB.Label Quadrado 
         BackColor       =   &H8000000D&
         Height          =   735
         Index           =   7
         Left            =   600
         TabIndex        =   11
         Top             =   960
         Width           =   735
      End
      Begin VB.Label lbl_q01 
         BackColor       =   &H80000005&
         Caption         =   "1. Clique no quadrado"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   2
         Left            =   240
         TabIndex        =   3
         Top             =   240
         Width           =   14055
      End
   End
   Begin VB.Frame frame_questao 
      BackColor       =   &H80000005&
      Height          =   2535
      Index           =   0
      Left            =   120
      TabIndex        =   8
      Top             =   2520
      Width           =   14535
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackColor       =   &H0000FF00&
         Caption         =   "INICIAR"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   6240
         TabIndex        =   10
         Top             =   1200
         Width           =   2175
      End
      Begin VB.Label lbl_q01 
         BackColor       =   &H80000005&
         Caption         =   "Clique em INICIAR para come�ar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Index           =   0
         Left            =   240
         TabIndex        =   9
         Top             =   240
         Width           =   14055
      End
   End
   Begin VB.Timer Tempo 
      Left            =   14520
      Top             =   720
   End
   Begin VB.Label lbl_descricao 
      BackColor       =   &H80000005&
      Caption         =   "Para realizar a matr�cula, responda corretamente todas as quest�es a sguir:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   120
      TabIndex        =   1
      Top             =   960
      Width           =   14295
   End
   Begin VB.Label lbl_titulo 
      BackColor       =   &H80000005&
      Caption         =   "Matr�cula"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   26.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   735
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   14175
   End
End
Attribute VB_Name = "frm05_matricula"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Sub Sleep Lib "kernel32.dll" (ByVal dwMilliseconds As Long)

Dim questao As Integer
Dim ordemTom As Integer

Private Sub dorme(ByVal questao As Integer, ByVal Tempo As Integer)
Dim Pos As Integer
  
  If questao = 1 Then
    For Pos = 0 To 1
      Me.lbl_q1_opcao(Pos).ForeColor = vbBlack
      Me.lbl_q1_opcao(Pos).Refresh
    Next
  ElseIf questao = 2 Then
    For Pos = 0 To 1
      Me.lbl_q2_opcao(Pos).ForeColor = vbBlack
      Me.lbl_q2_opcao(Pos).Refresh
    Next
  ElseIf questao = 3 Then
    For Pos = 0 To 1
      Me.lbl_q3_opcao(Pos).ForeColor = vbBlack
      Me.lbl_q3_opcao(Pos).Refresh
    Next
  End If
    
  Me.MousePointer = 11
  Sleep Tempo
  Me.MousePointer = 1
End Sub

Private Sub mostra_questao(ByVal q As Integer)
Dim Pos As Integer
Dim Num As Integer
Dim salva As Integer
  
  For Pos = 0 To 3
    If Pos = q Then
      Me.frame_questao(Pos).Visible = True
    Else
      Me.frame_questao(Pos).Visible = False
    End If
  Next
  
  For Pos = 1 To 7
    Randomize
    Num = Int(((6) * Rnd()) + 1)
    If Num <> Pos Then
      salva = Me.Quadrado(Pos).Left
      Me.Quadrado(Pos).Left = Me.Quadrado(Num).Left
      Me.Quadrado(Num).Left = salva
    End If
  Next
  
  For Pos = 1 To 10
    Randomize
    Num = Int(((10) * Rnd()) + 1)
    If Num <> Pos Then
      salva = Me.MenorQuadrado(Pos).Left
      Me.MenorQuadrado(Pos).Left = Me.MenorQuadrado(Num).Left
      Me.MenorQuadrado(Num).Left = salva
    End If
  Next
  
  For Pos = 1 To 12
    Randomize
    Num = Int(((12) * Rnd()) + 1)
    If Num <> Pos Then
      salva = Me.Tonalidade(Pos).Left
      Me.Tonalidade(Pos).Left = Me.Tonalidade(Num).Left
      Me.Tonalidade(Num).Left = salva
    End If
    Me.Tonalidade(Pos).Caption = ""
  Next
  
  DoEvents
End Sub

Private Sub gerar_questoes()
Dim Pos As Integer
  
  For Pos = 0 To 3
    Me.frame_questao(Pos).Left = 244
    Me.frame_questao(Pos).Top = 2520
  Next

  questao = 0
  mostra_questao questao
  
  DoEvents
End Sub

Private Sub Command1_Click()
  MsgBox "Fase " & Fase & " conclu�da!", vbInformation
  MudaFase Fase
End Sub

Private Sub Form_Load()
  gerar_questoes
End Sub

Private Sub Label1_Click()
  questao = 1
  mostra_questao questao
End Sub

Private Sub MenorQuadrado_Click(Index As Integer)
  If Index = 4 Then
    questao = questao + 1
    mostra_questao questao
    ordemTom = 1
  Else
    MsgBox "Resposta incorreta. Voc� ser� direcionado ao in�cio do exerc�cio.", vbCritical
    questao = 0
    mostra_questao questao
  End If
End Sub

Private Sub Quadrado_Click(Index As Integer)
  If Me.Quadrado(Index).Height = Me.Quadrado(Index).Width Then
    questao = questao + 1
    mostra_questao questao
  Else
    MsgBox "Resposta incorreta. Voc� ser� direcionado ao in�cio do exerc�cio.", vbCritical
    questao = 0
    mostra_questao questao
  End If
End Sub

Private Sub Tonalidade_Click(Index As Integer)
  If Index = ordemTom Then
    If Index = 12 Then
      Me.Tonalidade(Index).Caption = Index
      MsgBox "Fase " & Fase & " conclu�da!", vbInformation
      MudaFase Fase
    End If
    ordemTom = ordemTom + 1
    Me.Tonalidade(Index).Caption = Index
  Else
    MsgBox "Resposta incorreta. Voc� ser� direcionado ao in�cio do exerc�cio.", vbCritical
    questao = 0
    mostra_questao questao
  End If
End Sub
