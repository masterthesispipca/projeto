VERSION 5.00
Begin VB.Form frm08_proposta 
   AutoRedraw      =   -1  'True
   BackColor       =   &H80000005&
   Caption         =   "Fase 8"
   ClientHeight    =   8535
   ClientLeft      =   1755
   ClientTop       =   1350
   ClientWidth     =   17040
   LinkTopic       =   "Form1"
   OLEDropMode     =   1  'Manual
   ScaleHeight     =   8535
   ScaleWidth      =   17040
   Begin VB.CommandButton Command1 
      Caption         =   "Atalho"
      Height          =   375
      Left            =   16080
      TabIndex        =   7
      Top             =   120
      Width           =   855
   End
   Begin VB.Label lbl_orientador 
      BackColor       =   &H80000005&
      Caption         =   "Redes de Computadores e Sistemas Distribuídos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   20.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Index           =   4
      Left            =   9480
      TabIndex        =   6
      Top             =   7080
      Width           =   6855
   End
   Begin VB.Image img_banca 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   14
      Left            =   8400
      Stretch         =   -1  'True
      Tag             =   "Redes de Computadores e Sistemas Distribuídos"
      Top             =   7080
      Width           =   975
   End
   Begin VB.Image img_banca 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   13
      Left            =   7320
      Stretch         =   -1  'True
      Tag             =   "Redes de Computadores e Sistemas Distribuídos"
      Top             =   7080
      Width           =   975
   End
   Begin VB.Image img_banca 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   10
      Left            =   6240
      Stretch         =   -1  'True
      Tag             =   "Pesquisa Operacional"
      Top             =   5760
      Width           =   975
   End
   Begin VB.Label lbl_orientador 
      BackColor       =   &H80000005&
      Caption         =   "Pesquisa Operacional"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   20.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Index           =   3
      Left            =   9480
      TabIndex        =   5
      Top             =   5760
      Width           =   6855
   End
   Begin VB.Label lbl_orientador 
      BackColor       =   &H80000005&
      Caption         =   "Engenharia de Software e Linguagens de Programação"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   20.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Index           =   2
      Left            =   9480
      TabIndex        =   4
      Top             =   3120
      Width           =   6855
   End
   Begin VB.Label lbl_orientador 
      BackColor       =   &H80000005&
      Caption         =   "Inteligência Artificial"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   20.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Index           =   0
      Left            =   9480
      TabIndex        =   3
      Top             =   4440
      Width           =   6855
   End
   Begin VB.Image img_banca 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   12
      Left            =   8400
      Stretch         =   -1  'True
      Tag             =   "Pesquisa Operacional"
      Top             =   5760
      Width           =   975
   End
   Begin VB.Image img_banca 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   11
      Left            =   7320
      Stretch         =   -1  'True
      Tag             =   "Pesquisa Operacional"
      Top             =   5760
      Width           =   975
   End
   Begin VB.Image img_banca 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   9
      Left            =   8400
      Stretch         =   -1  'True
      Tag             =   "Inteligência Artificial"
      Top             =   4440
      Width           =   975
   End
   Begin VB.Image img_banca 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   8
      Left            =   7320
      Stretch         =   -1  'True
      Tag             =   "Inteligência Artificial"
      Top             =   4440
      Width           =   975
   End
   Begin VB.Image img_banca 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   7
      Left            =   6240
      Stretch         =   -1  'True
      Tag             =   "Inteligência Artificial"
      Top             =   4440
      Width           =   975
   End
   Begin VB.Image img_banca 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   6
      Left            =   8400
      Stretch         =   -1  'True
      Tag             =   "Engenharia de Software e Linguagens de Programação"
      Top             =   3120
      Width           =   975
   End
   Begin VB.Image img_banca 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   5
      Left            =   7320
      Stretch         =   -1  'True
      Tag             =   "Engenharia de Software e Linguagens de Programação"
      Top             =   3120
      Width           =   975
   End
   Begin VB.Image img_banca 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   4
      Left            =   6240
      Stretch         =   -1  'True
      Tag             =   "Engenharia de Software e Linguagens de Programação"
      Top             =   3120
      Width           =   975
   End
   Begin VB.Image img_banca 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   3
      Left            =   8400
      Stretch         =   -1  'True
      Tag             =   "Computação Gráfica e Processamento de Imagens"
      Top             =   1800
      Width           =   975
   End
   Begin VB.Image img_banca 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   1
      Left            =   6240
      Stretch         =   -1  'True
      Tag             =   "Computação Gráfica e Processamento de Imagens"
      Top             =   1800
      Width           =   975
   End
   Begin VB.Image img_banca 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   2
      Left            =   7320
      Stretch         =   -1  'True
      Tag             =   "Computação Gráfica e Processamento de Imagens"
      Top             =   1800
      Width           =   975
   End
   Begin VB.Label lbl_orientador 
      BackColor       =   &H80000005&
      Caption         =   "Computação Gráfica e Processamento de Imagens"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   20.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Index           =   1
      Left            =   9480
      TabIndex        =   0
      Top             =   1800
      Width           =   6855
   End
   Begin VB.Image img_orientador 
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   5
      Left            =   1320
      Picture         =   "frm08_proposta.frx":0000
      Stretch         =   -1  'True
      Tag             =   "Pesquisa Operacional"
      ToolTipText     =   "Jose Vicente Canto dos Santos"
      Top             =   1800
      Width           =   975
   End
   Begin VB.Image img_orientador 
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   3
      Left            =   240
      Picture         =   "frm08_proposta.frx":5010
      Stretch         =   -1  'True
      Tag             =   "Inteligência Artificial"
      ToolTipText     =   "Joao Francisco Valiati"
      Top             =   5760
      Width           =   975
   End
   Begin VB.Image img_orientador 
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   13
      Left            =   2400
      Picture         =   "frm08_proposta.frx":8F0F
      Stretch         =   -1  'True
      Tag             =   "Inteligência Artificial"
      ToolTipText     =   "Sandro Jose Rigo"
      Top             =   5760
      Width           =   975
   End
   Begin VB.Image img_orientador 
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   12
      Left            =   2400
      Picture         =   "frm08_proposta.frx":A62B
      Stretch         =   -1  'True
      Tag             =   "Redes de Computadores e Sistemas Distribuídos"
      ToolTipText     =   "Rodrigo da Rosa Righi"
      Top             =   4440
      Width           =   975
   End
   Begin VB.Image img_orientador 
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   11
      Left            =   2400
      Picture         =   "frm08_proposta.frx":F6AA
      Stretch         =   -1  'True
      Tag             =   "Inteligência Artificial"
      ToolTipText     =   "Patricia Augustin Jaques Maillard"
      Top             =   3120
      Width           =   975
   End
   Begin VB.Image img_orientador 
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   10
      Left            =   2400
      Picture         =   "frm08_proposta.frx":146A7
      Stretch         =   -1  'True
      Tag             =   "Computação Gráfica e Processamento de Imagens"
      ToolTipText     =   "Marta Becker Villamil"
      Top             =   1800
      Width           =   975
   End
   Begin VB.Image img_orientador 
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   9
      Left            =   1320
      Picture         =   "frm08_proposta.frx":195D9
      Stretch         =   -1  'True
      Tag             =   "Computação Gráfica e Processamento de Imagens"
      ToolTipText     =   "Luiz Paulo Luna de Oliveira"
      Top             =   7080
      Width           =   975
   End
   Begin VB.Image img_orientador 
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   7
      Left            =   1320
      Picture         =   "frm08_proposta.frx":1E471
      Stretch         =   -1  'True
      Tag             =   "Pesquisa Operacional"
      ToolTipText     =   "Leonardo Dagnino Chiwiacowsky"
      Top             =   4440
      Width           =   975
   End
   Begin VB.Image img_orientador 
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   6
      Left            =   1320
      Picture         =   "frm08_proposta.frx":22E49
      Stretch         =   -1  'True
      Tag             =   "Engenharia de Software e Linguagens de Programação"
      ToolTipText     =   "Kleinner Silva Farias de Oliveira"
      Top             =   3120
      Width           =   975
   End
   Begin VB.Image img_orientador 
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   2
      Left            =   240
      Picture         =   "frm08_proposta.frx":267CF
      Stretch         =   -1  'True
      Tag             =   "Engenharia de Software e Linguagens de Programação"
      ToolTipText     =   "Joao Carlos Gluz"
      Top             =   4440
      Width           =   975
   End
   Begin VB.Image img_orientador 
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   8
      Left            =   1320
      Picture         =   "frm08_proposta.frx":2B583
      Stretch         =   -1  'True
      Tag             =   "Computação Gráfica e Processamento de Imagens"
      ToolTipText     =   "Luiz Gonzaga da Silveira Junior"
      Top             =   5760
      Width           =   975
   End
   Begin VB.Image img_orientador 
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   1
      Left            =   240
      Picture         =   "frm08_proposta.frx":300B1
      Stretch         =   -1  'True
      Tag             =   "Redes de Computadores e Sistemas Distribuídos"
      ToolTipText     =   "Cristiano Andre da Costa"
      Top             =   3120
      Width           =   975
   End
   Begin VB.Image img_orientador 
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   4
      Left            =   240
      Picture         =   "frm08_proposta.frx":34FFC
      Stretch         =   -1  'True
      Tag             =   "Engenharia de Software e Linguagens de Programação"
      ToolTipText     =   "Jorge Luis Victoria Barbosa"
      Top             =   7080
      Width           =   975
   End
   Begin VB.Image img_orientador 
      DragMode        =   1  'Automatic
      Height          =   1215
      Index           =   0
      Left            =   240
      Picture         =   "frm08_proposta.frx":39ED4
      Stretch         =   -1  'True
      Tag             =   "Pesquisa Operacional"
      ToolTipText     =   "Arthur Torgo Gomez"
      Top             =   1800
      Width           =   975
   End
   Begin VB.Label lbl_descricao 
      BackColor       =   &H80000005&
      Caption         =   "Seleção da banca: arraste 3 professores conforme a linha de pesqusia"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   120
      TabIndex        =   2
      Top             =   960
      Width           =   16695
   End
   Begin VB.Label lbl_titulo 
      BackColor       =   &H80000005&
      Caption         =   "Apresentação da proposta"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   26.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   735
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   14175
   End
End
Attribute VB_Name = "frm08_proposta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim linhaPesquisa(5) As String
Dim contador As Integer
Dim linhaP As String
Dim orientador As Integer

Private Sub Verifica()
Dim Pos As Integer
Dim Ind As Integer
Dim Erro As Boolean

  Erro = False
  For Pos = 1 To 14
    If Me.img_banca(Pos).Visible = True Then
      Ind = Me.img_banca(Pos).ToolTipText
      If Me.img_banca(Pos).Tag <> Me.img_orientador(Ind).Tag Then
        Erro = True
        Exit For
      End If
    End If
  Next

  If Erro = True Then
    MsgBox "A relação não está correta.", vbCritical
    Prepara
  Else
    MsgBox "Fase " & Fase & " concluída!", vbInformation
    MudaFase Fase
  End If
End Sub

Private Sub Command1_Click()
  MsgBox "Fase " & Fase & " concluída!", vbInformation
  MudaFase Fase
End Sub

Private Sub Form_Load()
  Prepara
End Sub

Private Sub img_banca_DragDrop(Index As Integer, Source As Control, X As Single, Y As Single)
  Me.img_banca(Index).Picture = Source.Picture
  Me.img_banca(Index).ToolTipText = orientador
  contador = contador + 1
    
  If contador = 3 Then
    Verifica
  End If
End Sub

Private Sub Prepara()
Dim Total As Integer
Dim Pos As Integer
Dim Num As Integer

  For Pos = 1 To 14
    Me.img_banca(Pos).Picture = LoadPicture("")
    Me.img_banca(Pos).Visible = False
    Me.img_banca(Pos).ToolTipText = ""
  Next

  Total = 0
  Do While Total <= 2
    Randomize
    Num = Int(((14) * Rnd()) + 1)
    If Me.img_banca(Num).Visible = False Then
      Total = Total + 1
      Me.img_banca(Num).Visible = True
    End If
  Loop

  contador = 0
End Sub

Private Sub img_orientador_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
  orientador = Index
End Sub

