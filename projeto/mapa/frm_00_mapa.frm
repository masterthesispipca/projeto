VERSION 5.00
Begin VB.Form frm00_mapa 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   Caption         =   "Capture de Thesis"
   ClientHeight    =   9360
   ClientLeft      =   1125
   ClientTop       =   855
   ClientWidth     =   18255
   LinkTopic       =   "Form1"
   ScaleHeight     =   9360
   ScaleWidth      =   18255
   Begin VB.Label lbl_fase 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Defesa"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   300
      Index           =   8
      Left            =   2760
      TabIndex        =   7
      Top             =   480
      Width           =   855
   End
   Begin VB.Label lbl_fase 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Proposta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   300
      Index           =   7
      Left            =   8320
      TabIndex        =   6
      Top             =   840
      Width           =   1095
   End
   Begin VB.Label lbl_fase 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Proficiência"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   300
      Index           =   6
      Left            =   11440
      TabIndex        =   5
      Top             =   360
      Width           =   1335
   End
   Begin VB.Label lbl_fase 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Cadeiras"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   300
      Index           =   5
      Left            =   16005
      TabIndex        =   4
      Top             =   1200
      Width           =   975
   End
   Begin VB.Label lbl_fase 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Matrícula"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   300
      Index           =   4
      Left            =   16005
      TabIndex        =   3
      Top             =   6720
      Width           =   975
   End
   Begin VB.Label lbl_fase 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Orientador"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   300
      Index           =   3
      Left            =   11200
      TabIndex        =   2
      Top             =   6600
      Width           =   1215
   End
   Begin VB.Label lbl_fase 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Entrevista"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   300
      Index           =   2
      Left            =   7260
      TabIndex        =   1
      Top             =   7200
      Width           =   1215
   End
   Begin VB.Image img_personagem 
      Height          =   1335
      Left            =   360
      Picture         =   "frm_00_mapa.frx":0000
      Stretch         =   -1  'True
      Top             =   7920
      Width           =   855
   End
   Begin VB.Label lbl_indice 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      Caption         =   "5"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Index           =   5
      Left            =   15720
      TabIndex        =   15
      Top             =   1200
      Width           =   255
   End
   Begin VB.Label lbl_indice 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      Caption         =   "6"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Index           =   6
      Left            =   11160
      TabIndex        =   14
      Top             =   360
      Width           =   255
   End
   Begin VB.Label lbl_indice 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      Caption         =   "7"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Index           =   7
      Left            =   8040
      TabIndex        =   13
      Top             =   840
      Width           =   255
   End
   Begin VB.Label lbl_indice 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      Caption         =   "8"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Index           =   8
      Left            =   2400
      TabIndex        =   12
      Top             =   480
      Width           =   255
   End
   Begin VB.Label lbl_indice 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      Caption         =   "4"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Index           =   4
      Left            =   15720
      TabIndex        =   11
      Top             =   6720
      Width           =   255
   End
   Begin VB.Label lbl_indice 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      Caption         =   "3"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Index           =   3
      Left            =   10920
      TabIndex        =   10
      Top             =   6600
      Width           =   255
   End
   Begin VB.Label lbl_indice 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      Caption         =   "2"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Index           =   2
      Left            =   6960
      TabIndex        =   9
      Top             =   7200
      Width           =   255
   End
   Begin VB.Label lbl_indice 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      Caption         =   "1"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Index           =   1
      Left            =   2400
      TabIndex        =   8
      Top             =   6720
      Width           =   255
   End
   Begin VB.Shape brd_fase 
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      BorderWidth     =   2
      Height          =   255
      Index           =   8
      Left            =   2640
      Top             =   495
      Width           =   1095
   End
   Begin VB.Shape brd_fase 
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      BorderWidth     =   2
      Height          =   255
      Index           =   7
      Left            =   8280
      Top             =   855
      Width           =   1215
   End
   Begin VB.Shape brd_fase 
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      BorderWidth     =   2
      Height          =   255
      Index           =   6
      Left            =   11400
      Top             =   375
      Width           =   1575
   End
   Begin VB.Shape brd_fase 
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      BorderWidth     =   2
      Height          =   255
      Index           =   5
      Left            =   15960
      Top             =   1215
      Width           =   1215
   End
   Begin VB.Shape brd_fase 
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      BorderWidth     =   2
      Height          =   255
      Index           =   4
      Left            =   15960
      Top             =   6735
      Width           =   1215
   End
   Begin VB.Shape brd_fase 
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      BorderWidth     =   2
      Height          =   255
      Index           =   3
      Left            =   11160
      Top             =   6615
      Width           =   1335
   End
   Begin VB.Shape brd_fase 
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      BorderWidth     =   2
      Height          =   255
      Index           =   2
      Left            =   7200
      Top             =   7220
      Width           =   1335
   End
   Begin VB.Label lbl_fase 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Inscrição"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   300
      Index           =   1
      Left            =   2700
      TabIndex        =   0
      Top             =   6720
      Width           =   1095
   End
   Begin VB.Shape brd_fase 
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      BorderWidth     =   2
      Height          =   255
      Index           =   1
      Left            =   2640
      Top             =   6735
      Width           =   1215
   End
   Begin VB.Image mapa 
      Height          =   9375
      Left            =   0
      Picture         =   "frm_00_mapa.frx":43DF3
      Stretch         =   -1  'True
      Top             =   0
      Width           =   18255
   End
End
Attribute VB_Name = "frm00_mapa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Form_Load()
  Me.img_personagem.Picture = LoadPicture(App.Path & "\mapa\imagem\xhr" & personagem & ".gif")
End Sub

Private Sub img_personagem_Click()
  Select Case Fase
    Case 1: frm01_inscricao.Show
    Case 2: frm02_entrevista.Show
    Case 3: frm04_orientador.Show
    Case 4: frm05_matricula.Show
    Case 5: frm06_cursar_cadeiras.Show
    Case 6: frm07_proficiencia.Show
    Case 7: frm08_proposta.Show
    Case 8: frm09_defesa.Show
  End Select
End Sub
