VERSION 5.00
Begin VB.Form frm00_inicio 
   BackColor       =   &H00E0E0E0&
   Caption         =   "In�cio"
   ClientHeight    =   5235
   ClientLeft      =   7170
   ClientTop       =   2955
   ClientWidth     =   6990
   LinkTopic       =   "Form1"
   ScaleHeight     =   5235
   ScaleWidth      =   6990
   Begin VB.Timer Timer1 
      Left            =   6240
      Top             =   960
   End
   Begin VB.Image curti 
      Height          =   855
      Left            =   4440
      Picture         =   "frm00_inicio.frx":0000
      Stretch         =   -1  'True
      Top             =   1320
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Escolha seu personagem:"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   2
      Top             =   1920
      Width           =   3735
   End
   Begin VB.Label Label1 
      BackColor       =   &H8000000E&
      BackStyle       =   0  'Transparent
      Caption         =   "Bem vindo ao Master Thesis"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   27.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   10935
   End
   Begin VB.Image img_personagem 
      Height          =   2175
      Index           =   1
      Left            =   3800
      Picture         =   "frm00_inicio.frx":11D4
      Stretch         =   -1  'True
      Top             =   2640
      Width           =   1455
   End
   Begin VB.Image img_personagem 
      Height          =   2175
      Index           =   0
      Left            =   1800
      Picture         =   "frm00_inicio.frx":600F9
      Stretch         =   -1  'True
      Top             =   2640
      Width           =   1455
   End
   Begin VB.Label Label1 
      BackColor       =   &H8000000E&
      BackStyle       =   0  'Transparent
      Caption         =   "Bem vindo ao Master Thesis"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   27.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000A&
      Height          =   855
      Index           =   1
      Left            =   150
      TabIndex        =   1
      Top             =   140
      Width           =   10935
   End
   Begin VB.Image balao 
      Height          =   1455
      Left            =   3840
      Picture         =   "frm00_inicio.frx":A3EEC
      Stretch         =   -1  'True
      Top             =   1200
      Visible         =   0   'False
      Width           =   1575
   End
End
Attribute VB_Name = "frm00_inicio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
  Fase = 0
End Sub

Private Sub img_personagem_Click(Index As Integer)
  If Index = 0 Then
    Me.img_personagem(1).Visible = False
    personagem = 0
  Else
    Me.img_personagem(0).Visible = False
    personagem = 1
  End If
  
  Me.img_personagem(personagem).Left = 2880
  Me.balao.Visible = True
  Me.curti.Visible = True
  Me.Timer1.Interval = 2000
End Sub

Private Sub Timer1_Timer()
  frm00_mapa.Show
  MudaFase Fase
End Sub
