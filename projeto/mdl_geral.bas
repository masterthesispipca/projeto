Attribute VB_Name = "Module1"
Public personagem As Integer
Public Fase As Integer

Public Sub MudaFase(ByVal f As Integer)
  If f <= 7 Then
    frm00_mapa.img_personagem.Top = frm00_mapa.lbl_indice(f + 1).Top + 320
    frm00_mapa.img_personagem.Left = frm00_mapa.lbl_indice(f + 1).Left + 360
    frm00_mapa.Refresh
  End If
  Select Case f
    Case 0: Unload frm00_inicio
    Case 1: Unload frm01_inscricao
    Case 2: Unload frm02_entrevista
    Case 3: Unload frm04_orientador
    Case 4: Unload frm05_matricula
    Case 5: Unload frm06_cursar_cadeiras
    Case 6: Unload frm07_proficiencia
    Case 7: Unload frm08_proposta
    Case 8: Unload frm09_defesa
  End Select
  Fase = Fase + 1
  If Fase > 8 Then frm10_final.Show
End Sub

