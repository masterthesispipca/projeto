VERSION 5.00
Begin VB.Form frm07_proficiencia 
   BackColor       =   &H80000005&
   Caption         =   "Fase 6"
   ClientHeight    =   6270
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   15060
   LinkTopic       =   "Form1"
   ScaleHeight     =   6270
   ScaleWidth      =   15060
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Atalho"
      Height          =   375
      Left            =   14040
      TabIndex        =   8
      Top             =   120
      Width           =   855
   End
   Begin VB.Frame frame_questao 
      BackColor       =   &H80000005&
      Height          =   3255
      Index           =   2
      Left            =   240
      TabIndex        =   2
      Top             =   2520
      Width           =   14535
      Begin VB.Label lbl_cor 
         BackStyle       =   0  'Transparent
         Caption         =   "green"
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   24
            Charset         =   0
            Weight          =   900
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   4
         Left            =   9840
         TabIndex        =   7
         Top             =   2280
         Width           =   2775
      End
      Begin VB.Label lbl_cor 
         BackStyle       =   0  'Transparent
         Caption         =   "blue"
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   24
            Charset         =   0
            Weight          =   900
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   3
         Left            =   6720
         TabIndex        =   6
         Top             =   1440
         Width           =   2775
      End
      Begin VB.Label lbl_cor 
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "red"
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   24
            Charset         =   0
            Weight          =   900
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   2
         Left            =   3600
         TabIndex        =   5
         Top             =   1920
         Width           =   2775
      End
      Begin VB.Label lbl_cor 
         BackColor       =   &H8000000B&
         BackStyle       =   0  'Transparent
         Caption         =   "yellow"
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   24
            Charset         =   0
            Weight          =   900
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   1
         Left            =   600
         TabIndex        =   4
         Top             =   960
         Width           =   2655
      End
      Begin VB.Label lbl_q01 
         BackColor       =   &H80000005&
         Caption         =   "Click the color that corresponds to writing:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   2
         Left            =   240
         TabIndex        =   3
         Top             =   240
         Width           =   10215
      End
   End
   Begin VB.Timer Tempo 
      Interval        =   1000
      Left            =   14640
      Top             =   720
   End
   Begin VB.Label lbl_descricao 
      BackColor       =   &H80000005&
      Caption         =   $"frm07_proficiencia.frx":0000
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   120
      TabIndex        =   1
      Top             =   960
      Width           =   14775
   End
   Begin VB.Label lbl_titulo 
      BackColor       =   &H80000005&
      Caption         =   "Prova de profici�ncia em l�ngua estrangeira"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   26.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   735
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   14175
   End
End
Attribute VB_Name = "frm07_proficiencia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim q02_posicao(4) As Integer
Dim cor(7, 2) As Variant

Private Sub Command1_Click()
  MsgBox "Fase " & Fase & " conclu�da!", vbInformation
  MudaFase Fase
End Sub

Private Sub Form_Load()
Dim Num As Integer

  q02_posicao(1) = 960
  q02_posicao(2) = 1440
  q02_posicao(3) = 1920
  q02_posicao(4) = 2400
  
  cor(1, 1) = "red"
  cor(1, 2) = 255
  cor(2, 1) = "green"
  cor(2, 2) = 65280
  cor(3, 1) = "yellow"
  cor(3, 2) = 65535
  cor(4, 1) = "blue"
  cor(4, 2) = 16711680
  cor(5, 1) = "magenta"
  cor(5, 2) = 16711935
  cor(6, 1) = "cyan"
  cor(6, 2) = 16776960
  cor(7, 1) = "black"
  cor(7, 2) = 0
End Sub

Private Sub lbl_cor_Click(Index As Integer)
  For Pos = 1 To 7
    If Me.lbl_cor(Index).Caption = cor(Pos, 1) And Me.lbl_cor(Index).ForeColor = cor(Pos, 2) Then
      MsgBox "Fase " & Fase & " conclu�da!", vbInformation
      MudaFase Fase
      Exit For
    End If
  Next
End Sub

Private Sub Tempo_Timer()
Dim Pos As Integer
Dim atual As Integer
Dim salva As Integer

    For Pos = 1 To 4
      Randomize
      atual = Int(((7) * Rnd()) + 1)
      Me.lbl_cor(Pos).Caption = cor(atual, 1)
       
      Randomize
      atual = Int(((7) * Rnd()) + 1)
      Me.lbl_cor(Pos).ForeColor = cor(atual, 2)
    
      Randomize
      atual = Int(((4) * Rnd()) + 1)
      Me.lbl_cor(Pos).Top = q02_posicao(atual)
    Next
    
  DoEvents
End Sub
