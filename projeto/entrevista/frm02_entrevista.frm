VERSION 5.00
Begin VB.Form frm02_entrevista 
   BackColor       =   &H80000005&
   Caption         =   "Fase 2"
   ClientHeight    =   5310
   ClientLeft      =   2775
   ClientTop       =   1980
   ClientWidth     =   15030
   LinkTopic       =   "Form1"
   ScaleHeight     =   5310
   ScaleWidth      =   15030
   Begin VB.CommandButton Command1 
      Caption         =   "Atalho"
      Height          =   375
      Left            =   14040
      TabIndex        =   17
      Top             =   120
      Width           =   855
   End
   Begin VB.Frame frame_questao 
      BackColor       =   &H80000005&
      Height          =   2535
      Index           =   3
      Left            =   480
      TabIndex        =   10
      Top             =   4320
      Width           =   14535
      Begin VB.Label lbl_q3_opcao 
         Alignment       =   2  'Center
         BackColor       =   &H0000FF00&
         Caption         =   "N"
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   20.25
            Charset         =   0
            Weight          =   900
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   615
         Index           =   1
         Left            =   2520
         TabIndex        =   13
         Top             =   1440
         Width           =   1815
      End
      Begin VB.Label lbl_q3_opcao 
         Alignment       =   2  'Center
         BackColor       =   &H0000FF00&
         Caption         =   "S"
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   20.25
            Charset         =   0
            Weight          =   900
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   615
         Index           =   0
         Left            =   240
         TabIndex        =   12
         Top             =   1440
         Width           =   1815
      End
      Begin VB.Label lbl_q03 
         BackColor       =   &H80000005&
         Caption         =   "3. Voc� se prop�e a escrever 119 artigos antes de apresentar a proposta da disserta��o?"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Index           =   1
         Left            =   240
         TabIndex        =   11
         Top             =   240
         Width           =   14055
      End
   End
   Begin VB.Frame frame_questao 
      BackColor       =   &H80000005&
      Height          =   2535
      Index           =   1
      Left            =   240
      TabIndex        =   2
      Top             =   3120
      Width           =   14535
      Begin VB.Label lbl_q1_opcao 
         Alignment       =   2  'Center
         BackColor       =   &H0000FF00&
         Caption         =   "N"
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   20.25
            Charset         =   0
            Weight          =   900
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   615
         Index           =   1
         Left            =   2520
         TabIndex        =   5
         Top             =   1440
         Width           =   1815
      End
      Begin VB.Label lbl_q1_opcao 
         Alignment       =   2  'Center
         BackColor       =   &H0000FF00&
         Caption         =   "S"
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   20.25
            Charset         =   0
            Weight          =   900
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   615
         Index           =   0
         Left            =   240
         TabIndex        =   4
         Top             =   1440
         Width           =   1815
      End
      Begin VB.Label lbl_q01 
         BackColor       =   &H80000005&
         Caption         =   "1. Seu tempo livre para dedica��o ao mestrado �, pelo menos, 29.5 horas por dia, inclusive finais de semana e feriados?"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Index           =   2
         Left            =   240
         TabIndex        =   3
         Top             =   240
         Width           =   14055
      End
   End
   Begin VB.Frame frame_questao 
      BackColor       =   &H80000005&
      Height          =   2535
      Index           =   2
      Left            =   360
      TabIndex        =   6
      Top             =   3720
      Width           =   14535
      Begin VB.Label lbl_q02 
         BackColor       =   &H80000005&
         Caption         =   "2. Voc� conhece todas as linguagens de programa��o, inclusive aquelas que ainda nem foram inventadas?"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Index           =   0
         Left            =   240
         TabIndex        =   9
         Top             =   240
         Width           =   14055
      End
      Begin VB.Label lbl_q2_opcao 
         Alignment       =   2  'Center
         BackColor       =   &H0000FF00&
         Caption         =   "S"
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   20.25
            Charset         =   0
            Weight          =   900
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   615
         Index           =   0
         Left            =   240
         TabIndex        =   8
         Top             =   1440
         Width           =   1815
      End
      Begin VB.Label lbl_q2_opcao 
         Alignment       =   2  'Center
         BackColor       =   &H0000FF00&
         Caption         =   "N"
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   20.25
            Charset         =   0
            Weight          =   900
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   615
         Index           =   1
         Left            =   2520
         TabIndex        =   7
         Top             =   1440
         Width           =   1815
      End
   End
   Begin VB.Frame frame_questao 
      BackColor       =   &H80000005&
      Height          =   2535
      Index           =   0
      Left            =   120
      TabIndex        =   14
      Top             =   2520
      Width           =   14535
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackColor       =   &H0000FF00&
         Caption         =   "INICIAR"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   6240
         TabIndex        =   16
         Top             =   1200
         Width           =   2175
      End
      Begin VB.Label lbl_q01 
         BackColor       =   &H80000005&
         Caption         =   "Clique em INICIAR para come�ar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Index           =   0
         Left            =   240
         TabIndex        =   15
         Top             =   240
         Width           =   14055
      End
   End
   Begin VB.Timer Tempo 
      Left            =   14520
      Top             =   840
   End
   Begin VB.Label lbl_descricao 
      BackColor       =   &H80000005&
      Caption         =   "Para passar pela entrevista, responda SIM para todas as perguntas do questin�rio a seguir:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   120
      TabIndex        =   1
      Top             =   960
      Width           =   14295
   End
   Begin VB.Label lbl_titulo 
      BackColor       =   &H80000005&
      Caption         =   "Entrevista"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   26.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   735
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   14175
   End
End
Attribute VB_Name = "frm02_entrevista"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Sub Sleep Lib "kernel32.dll" (ByVal dwMilliseconds As Long)

Dim questao As Integer

Private Sub dorme(ByVal questao As Integer, ByVal Tempo As Integer)
Dim Pos As Integer
  
  If questao = 1 Then
    For Pos = 0 To 1
      Me.lbl_q1_opcao(Pos).ForeColor = vbBlack
      Me.lbl_q1_opcao(Pos).Refresh
    Next
  ElseIf questao = 2 Then
    For Pos = 0 To 1
      Me.lbl_q2_opcao(Pos).ForeColor = vbBlack
      Me.lbl_q2_opcao(Pos).Refresh
    Next
  ElseIf questao = 3 Then
    For Pos = 0 To 1
      Me.lbl_q3_opcao(Pos).ForeColor = vbBlack
      Me.lbl_q3_opcao(Pos).Refresh
    Next
  End If
    
  Me.MousePointer = 11
  Sleep Tempo
  Me.MousePointer = 1
End Sub

Private Sub mostra_questao(ByVal q As Integer)
Dim Pos As Integer
  
  Me.lbl_q1_opcao(0).ForeColor = vbGreen
  Me.lbl_q1_opcao(1).ForeColor = vbGreen
  Me.lbl_q2_opcao(0).ForeColor = vbGreen
  Me.lbl_q2_opcao(1).ForeColor = vbGreen
  Me.lbl_q3_opcao(0).ForeColor = vbGreen
  Me.lbl_q3_opcao(1).ForeColor = vbGreen
  Me.Refresh
  
  For Pos = 0 To 3
    If Pos = q Then
      Me.frame_questao(Pos).Visible = True
    Else
      Me.frame_questao(Pos).Visible = False
    End If
  Next
  
  DoEvents
End Sub

Private Sub gerar_questoes()
Dim Pos As Integer
  
  For Pos = 0 To 3
    Me.frame_questao(Pos).Left = 244
    Me.frame_questao(Pos).Top = 2520
  Next

  Randomize
  q1 = Int(((2) * Rnd()) + 1)
  If q1 = 1 Then
    Me.lbl_q1_opcao(0).Caption = "SIM"
    Me.lbl_q1_opcao(1).Caption = "N�O"
  Else
    Me.lbl_q1_opcao(1).Caption = "SIM"
    Me.lbl_q1_opcao(0).Caption = "N�O"
  End If

  Randomize
  q2 = Int(((2) * Rnd()) + 1)
  If q2 = 1 Then
    Me.lbl_q2_opcao(0).Caption = "SIM"
    Me.lbl_q2_opcao(1).Caption = "N�O"
  Else
    Me.lbl_q2_opcao(1).Caption = "SIM"
    Me.lbl_q2_opcao(0).Caption = "N�O"
  End If

  Randomize
  q3 = Int(((2) * Rnd()) + 1)
  If q3 = 1 Then
    Me.lbl_q3_opcao(0).Caption = "SIM"
    Me.lbl_q3_opcao(1).Caption = "N�O"
  Else
    Me.lbl_q3_opcao(1).Caption = "SIM"
    Me.lbl_q3_opcao(0).Caption = "N�O"
  End If

  questao = 0
  mostra_questao questao
  
  DoEvents
End Sub

Private Sub Command1_Click()
  MsgBox "Fase " & Fase & " conclu�da!", vbInformation
  MudaFase Fase
End Sub

Private Sub Form_Load()
  gerar_questoes
End Sub

Private Sub Label1_Click()
  questao = 1
  mostra_questao questao
End Sub

Private Sub lbl_q1_opcao_Click(Index As Integer)
  dorme questao, 1000
  If Me.lbl_q1_opcao(Index).Caption = "SIM" Then
    questao = questao + 1
    mostra_questao questao
  Else
    MsgBox "Resposta incorreta. Voc� ser� direcionado ao in�cio do exerc�cio.", vbCritical
    gerar_questoes
    questao = 0
    mostra_questao questao
  End If
End Sub

Private Sub lbl_q2_opcao_Click(Index As Integer)
  dorme questao, 1000
  If Me.lbl_q2_opcao(Index).Caption = "SIM" Then
    questao = questao + 1
    mostra_questao questao
  Else
    MsgBox "Resposta incorreta. Voc� ser� direcionado ao in�cio do exerc�cio.", vbCritical
    gerar_questoes
    questao = 0
    mostra_questao questao
  End If
End Sub

Private Sub lbl_q3_opcao_Click(Index As Integer)
  dorme questao, 1000
  If Me.lbl_q3_opcao(Index).Caption = "SIM" Then
    MsgBox "Fase " & Fase & " conclu�da!", vbInformation
    MudaFase Fase
  Else
    MsgBox "Resposta incorreta. Voc� ser� direcionado ao in�cio do exerc�cio.", vbCritical
    gerar_questoes
    questao = 0
    mostra_questao questao
  End If
End Sub

