VERSION 5.00
Begin VB.Form frm06_cursar_cadeiras 
   BackColor       =   &H80000005&
   Caption         =   "Fase 5"
   ClientHeight    =   6885
   ClientLeft      =   2805
   ClientTop       =   2175
   ClientWidth     =   15060
   LinkTopic       =   "Form1"
   ScaleHeight     =   6885
   ScaleWidth      =   15060
   Begin VB.Frame frame_questao 
      BackColor       =   &H80000005&
      Height          =   4815
      Index           =   3
      Left            =   480
      TabIndex        =   7
      Top             =   3360
      Width           =   14655
      Begin VB.Label lbl_q3 
         BackColor       =   &H8000000E&
         Caption         =   "d) 10 - 2 * 5 / 2 -1 = 40"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   3
         Left            =   960
         TabIndex        =   14
         Top             =   4080
         Width           =   4095
      End
      Begin VB.Label lbl_q3 
         BackColor       =   &H8000000E&
         Caption         =   "c) 46 - 8 / 3 * 6 + (2 - 7) * 5 * 6 * 2 = -270"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   2
         Left            =   960
         TabIndex        =   13
         Top             =   3360
         Width           =   6975
      End
      Begin VB.Label lbl_q3 
         BackColor       =   &H8000000E&
         Caption         =   "b) -54 * 3 - (19 + 4 - 32 / 4 * -2 + (54 / 6 - 4)) = -206"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   1
         Left            =   960
         TabIndex        =   12
         Top             =   2640
         Width           =   8655
      End
      Begin VB.Label lbl_q3 
         BackColor       =   &H8000000E&
         Caption         =   "a) 29 - (13 - 12 * 25) + 7 / 7 = 317"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   0
         Left            =   960
         TabIndex        =   11
         Top             =   1920
         Width           =   5775
      End
      Begin VB.Label lbl_verdadeiro 
         BackColor       =   &H80000005&
         Caption         =   "VERDADEIRO:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   495
         Index           =   4
         Left            =   1800
         TabIndex        =   10
         Top             =   1200
         Width           =   2295
      End
      Begin VB.Label lbl_q01 
         BackColor       =   &H80000005&
         Caption         =   "Clique no"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   3
         Left            =   240
         TabIndex        =   9
         Top             =   1200
         Width           =   1695
      End
      Begin VB.Label lbl_q01 
         BackColor       =   &H80000005&
         Caption         =   $"frm06_cursar_cadeiras.frx":0000
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   1
         Left            =   240
         TabIndex        =   8
         Top             =   240
         Width           =   14055
      End
   End
   Begin VB.Frame frame_questao 
      BackColor       =   &H80000005&
      Height          =   4815
      Index           =   1
      Left            =   240
      TabIndex        =   1
      Top             =   2160
      Width           =   14655
      Begin VB.Label lbl_ms_contador 
         Alignment       =   2  'Center
         BackColor       =   &H80000005&
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6720
         TabIndex        =   4
         Top             =   2760
         Width           =   1215
      End
      Begin VB.Image img_powerpoint 
         Height          =   975
         Left            =   6720
         Picture         =   "frm06_cursar_cadeiras.frx":00A3
         Stretch         =   -1  'True
         Top             =   1680
         Width           =   1215
      End
      Begin VB.Label lbl_q01 
         BackColor       =   &H80000005&
         Caption         =   $"frm06_cursar_cadeiras.frx":2956
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1455
         Index           =   2
         Left            =   240
         TabIndex        =   2
         Top             =   240
         Width           =   14055
      End
   End
   Begin VB.Frame frame_questao 
      BackColor       =   &H80000005&
      Height          =   4815
      Index           =   2
      Left            =   360
      TabIndex        =   5
      Top             =   2760
      Width           =   14655
      Begin VB.Image img_branco 
         Height          =   975
         Index           =   1
         Left            =   120
         Picture         =   "frm06_cursar_cadeiras.frx":2A54
         Stretch         =   -1  'True
         Top             =   3240
         Width           =   1215
      End
      Begin VB.Image img_branco 
         Height          =   975
         Index           =   2
         Left            =   120
         Picture         =   "frm06_cursar_cadeiras.frx":1BE7B
         Stretch         =   -1  'True
         Top             =   2160
         Width           =   1215
      End
      Begin VB.Image img_branco 
         Height          =   975
         Index           =   3
         Left            =   120
         Picture         =   "frm06_cursar_cadeiras.frx":352A2
         Stretch         =   -1  'True
         Top             =   1080
         Width           =   1215
      End
      Begin VB.Image img_remedio 
         Height          =   615
         Index           =   2
         Left            =   13200
         Picture         =   "frm06_cursar_cadeiras.frx":4E6C9
         Stretch         =   -1  'True
         Top             =   3240
         Width           =   1215
      End
      Begin VB.Image img_remedio 
         Height          =   975
         Index           =   1
         Left            =   9720
         Picture         =   "frm06_cursar_cadeiras.frx":506F3
         Stretch         =   -1  'True
         Top             =   2160
         Width           =   975
      End
      Begin VB.Image img_remedio 
         Height          =   855
         Index           =   3
         Left            =   13200
         Picture         =   "frm06_cursar_cadeiras.frx":6BAAD
         Stretch         =   -1  'True
         Top             =   2280
         Width           =   1095
      End
      Begin VB.Label lbl_q01 
         BackColor       =   &H80000005&
         Caption         =   $"frm06_cursar_cadeiras.frx":70CC5
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1455
         Index           =   0
         Left            =   240
         TabIndex        =   6
         Top             =   240
         Width           =   14055
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Atalho"
      Height          =   375
      Left            =   14040
      TabIndex        =   18
      Top             =   120
      Width           =   855
   End
   Begin VB.Timer Tempo 
      Interval        =   1000
      Left            =   14520
      Top             =   600
   End
   Begin VB.Frame frame_questao 
      BackColor       =   &H80000005&
      Height          =   4815
      Index           =   0
      Left            =   120
      TabIndex        =   15
      Top             =   1920
      Width           =   14655
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackColor       =   &H0000FF00&
         Caption         =   "INICIAR"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   6000
         TabIndex        =   17
         Top             =   1440
         Width           =   2175
      End
      Begin VB.Label lbl_q01 
         BackColor       =   &H80000005&
         Caption         =   "Clique em iniciar para come�ar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   4
         Left            =   240
         TabIndex        =   16
         Top             =   240
         Width           =   14055
      End
   End
   Begin VB.Label lbl_descricao 
      BackColor       =   &H80000005&
      Caption         =   "Execute as atividades de cada cadeira, conforme o perfil do professor, para concluir a primeira fase do mestrado."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   120
      TabIndex        =   3
      Top             =   960
      Width           =   14655
   End
   Begin VB.Label lbl_titulo 
      BackColor       =   &H80000005&
      Caption         =   "Cursar as cadeiras"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   26.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   735
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   14175
   End
End
Attribute VB_Name = "frm06_cursar_cadeiras"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Sub Sleep Lib "kernel32.dll" (ByVal dwMilliseconds As Long)

Dim q2_mapa(11, 3) As Long
Dim sm_contador As Integer
Dim questao As Integer
Dim tp_contador As Integer

Private Sub Inicializa()
  sm_contador = 0
  tp_contador = 0
  Me.lbl_ms_contador.Caption = 0
  sorteia_localizacao

  For Pos = 1 To 3
    Me.frame_questao(Pos).Top = 2160
    Me.frame_questao(Pos).Left = 240
    Me.img_branco(Pos).Visible = True
  Next
  
  gerar_questoes
End Sub

Private Sub dorme(ByVal Tempo As Integer)
  Me.MousePointer = 11
  Sleep 1000
  Me.MousePointer = 1
End Sub

Private Sub sorteia_localizacao()
Dim c As Integer
Dim l As Integer
Dim Esquerda As Long
Dim topo As Long
Dim contador As Integer

  For c = 1 To 11
    For l = 1 To 3
      q2_mapa(c, l) = 0
    Next
  Next

  contador = 0
  Do While contador <= 2
    Randomize
    Esquerda = Int(((11) * Rnd()) + 1)
    Randomize
    topo = Int(((3) * Rnd()) + 1)
    
    If q2_mapa(Esquerda, topo) = 0 Then
      q2_mapa(Esquerda, topo) = 1
      contador = contador + 1
      Me.img_remedio(contador).Left = (120 + ((Esquerda - 1) * 1320))
      Me.img_remedio(contador).Top = 1080 * topo
      Me.img_branco(contador).Left = Me.img_remedio(contador).Left
      Me.img_branco(contador).Top = Me.img_remedio(contador).Top
    End If
  Loop

End Sub

Private Sub Command1_Click()
  MsgBox "Fase " & Fase & " conclu�da!", vbInformation
  MudaFase Fase
End Sub

Private Sub Form_Load()
  gerar_questoes
End Sub

Private Sub img_branco_Click(Index As Integer)
  Me.img_branco(Index).Visible = False
  DoEvents
  tp_contador = tp_contador + 1
  If tp_contador >= 3 Then
    dorme 2000
    questao = 3
    mostra_questao questao
  End If
End Sub

Private Sub gerar_questoes()
Dim Pos As Integer
  
  For Pos = 0 To 3
    Me.frame_questao(Pos).Left = 244
    Me.frame_questao(Pos).Top = 1920
  Next

  questao = 0
  mostra_questao questao
  
  DoEvents
End Sub

Private Sub mostra_questao(ByVal q As Integer)
Dim Pos As Integer
  
  For Pos = 0 To 3
    If Pos = q Then
      Me.frame_questao(Pos).Visible = True
    Else
      Me.frame_questao(Pos).Visible = False
    End If
  Next
  
  DoEvents
End Sub

Private Sub img_powerpoint_Click()
Dim Esquerda As Integer

  sm_contador = sm_contador + 1
  Me.lbl_ms_contador.Caption = sm_contador
  Me.lbl_ms_contador.Refresh
  
  If sm_contador Mod 6 = 0 Then
    Randomize
    Esquerda = Int(((11) * Rnd()) + 1)
    Me.img_powerpoint.Left = (120 + ((Esquerda - 1) * 1320))
    Me.lbl_ms_contador.Left = Me.img_powerpoint.Left
  End If
  
  If sm_contador >= 30 Then
    questao = 2
    mostra_questao questao
  End If
End Sub

Private Sub Label1_Click()
  Inicializa
  questao = 1
  mostra_questao questao
End Sub

Private Sub lbl_q3_Click(Index As Integer)
  MsgBox "Resposta incorreta. Voc� ser� direcionado ao in�cio do exerc�cio.", vbCritical
  questao = 0
  mostra_questao questao
End Sub

Private Sub lbl_verdadeiro_Click(Index As Integer)
  MsgBox "Fase " & Fase & " conclu�da!", vbInformation
  MudaFase Fase
End Sub
