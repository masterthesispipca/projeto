VERSION 5.00
Begin VB.Form frm02_entrevista 
   BackColor       =   &H80000005&
   Caption         =   "Entrevista para mestrado"
   ClientHeight    =   7410
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   15030
   LinkTopic       =   "Form1"
   ScaleHeight     =   7410
   ScaleWidth      =   15030
   StartUpPosition =   3  'Windows Default
   Begin VB.Label lbl_titulo 
      BackColor       =   &H80000005&
      Caption         =   "Responda as perguntas da entrevista"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   26.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   735
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   14175
   End
End
Attribute VB_Name = "frm02_entrevista"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Sub Sleep Lib "kernel32.dll" (ByVal dwMilliseconds As Long)

Const numCartas = 10
Dim carta(10) As String * 1
Dim posicao(10, 2) As Long
Dim carta1 As Integer
Dim carta2 As Integer

Private Sub doc_Click(Index As Integer)

  If carta(Index) = "S" Then
    Exit Sub
  End If

  If carta1 = 0 Then
    carta1 = Index
    mostraCarta carta1
  ElseIf carta2 = 0 Then
    carta2 = Index
    mostraCarta carta2
  End If
  
  DoEvents
  If carta1 <> 0 And carta2 <> 0 Then
     verificaPares carta1, carta2
  End If
End Sub

Private Sub Form_Load()
Dim num As Integer

  For num = 1 To numCartas
    carta(num) = "N"
    posicao(num, 1) = Me.doc(num).Left
    posicao(num, 2) = Me.doc(num).Top
  Next

  carta1 = 0
  carta2 = 0
  
  Me.MousePointer = 11
  Sleep 1000 'espera 2s para mostrar movimento no grafo
  Me.MousePointer = 1
  
  DoEvents
  embaralhaCartas
End Sub

Private Sub verificaPares(ByVal c1 As Integer, ByVal c2 As Integer)

  If Abs(c2 - c1) = 5 Then
    carta(c1) = "S"
    carta(c2) = "S"
    Me.doc(c1).Picture = LoadPicture(App.Path & "\imagem\entrevista\doc" & c1 & ".jpg")
    Me.doc(c2).Picture = LoadPicture(App.Path & "\imagem\entrevista\doc" & c2 & ".jpg")
  Else
    'MsgBox "Errou"
    Me.MousePointer = 11
    Sleep 1000 'espera 2s para mostrar movimento no grafo
    Me.MousePointer = 1

    Me.doc(c1).Picture = LoadPicture(App.Path & "\imagem\entrevista\charada.jpg")
    Me.doc(c2).Picture = LoadPicture(App.Path & "\imagem\entrevista\charada.jpg")
  End If

  If Not verificaFim Then
    carta1 = 0
    carta2 = 0
    embaralhaCartas
  Else
    MsgBox "Parab�ns!"
  End If
End Sub

Private Function verificaFim() As Boolean
Dim pos As Integer
  
  verificaFim = True
  For pos = 1 To numCartas
    If carta(pos) = "N" Then
      verificaFim = False
      Exit For
    End If
  Next
End Function

Private Sub embaralhaCartas()
Dim vetCartas1(10) As Integer
Dim vetCartas2(10) As Integer
Dim esquerda As Long
Dim topo As Long
Dim proximo As Integer
Dim atual As Integer
Dim salva As Integer
Dim ordem As Integer
Dim pos As Integer

  ordem = 0
  For num = 1 To 10
    If carta(num) = "N" Then
      ordem = ordem + 1
      vetCartas1(ordem) = num
      vetCartas2(ordem) = num
    End If
  Next

  For pos = 1 To ordem
    Randomize
    atual = Int(((ordem) * Rnd()) + 1)
    proximo = Int(((ordem) * Rnd()) + 1)
     
    salva = vetCartas2(atual)
    vetCartas2(atual) = vetCartas2(proximo)
    vetCartas2(proximo) = salva
  Next

  For pos = 1 To ordem
    esquerda = Me.doc(vetCartas1(pos)).Left
    topo = Me.doc(vetCartas1(pos)).Top
    Me.doc(vetCartas1(pos)).Left = Me.doc(vetCartas2(pos)).Left
    Me.doc(vetCartas1(pos)).Top = Me.doc(vetCartas2(pos)).Top
    Me.doc(vetCartas2(pos)).Left = esquerda
    Me.doc(vetCartas2(pos)).Top = topo
  Next
End Sub

Private Sub mostraCarta(ByVal c1 As Integer)
  Me.doc(c1).Picture = LoadPicture(App.Path & "\imagem\entrevista\doc" & c1 & ".jpg")
End Sub
