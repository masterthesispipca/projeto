VERSION 5.00
Begin VB.Form frm09_defesa 
   AutoRedraw      =   -1  'True
   BackColor       =   &H80000005&
   Caption         =   "Fase 8"
   ClientHeight    =   7215
   ClientLeft      =   3660
   ClientTop       =   1500
   ClientWidth     =   13020
   LinkTopic       =   "Form1"
   OLEDropMode     =   1  'Manual
   ScaleHeight     =   7215
   ScaleWidth      =   13020
   Begin VB.CommandButton Command1 
      Caption         =   "Atalho"
      Height          =   375
      Left            =   12000
      TabIndex        =   309
      Top             =   120
      Width           =   855
   End
   Begin VB.Timer Tempo 
      Left            =   11520
      Top             =   1800
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   180
      Index           =   301
      Left            =   3840
      TabIndex        =   304
      Top             =   6720
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   300
      Left            =   3840
      TabIndex        =   303
      Top             =   6720
      Width           =   735
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   299
      Left            =   8640
      TabIndex        =   302
      Top             =   4080
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   19
      Left            =   8400
      TabIndex        =   301
      Top             =   4080
      Width           =   60
   End
   Begin VB.Label Inicio 
      Alignment       =   2  'Center
      BackColor       =   &H00008000&
      Caption         =   "Iniciar"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000B&
      Height          =   435
      Left            =   1440
      TabIndex        =   300
      Top             =   2520
      Width           =   975
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   298
      Left            =   1320
      TabIndex        =   299
      Top             =   3120
      Width           =   855
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   900
      Index           =   297
      Left            =   1320
      TabIndex        =   298
      Top             =   2280
      Width           =   60
   End
   Begin VB.Image img_diploma 
      Height          =   1035
      Left            =   10920
      Picture         =   "frm09_defesa.frx":0000
      Stretch         =   -1  'True
      Top             =   6000
      Width           =   1815
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   27
      Left            =   9840
      TabIndex        =   297
      Top             =   5760
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   296
      Left            =   4560
      TabIndex        =   296
      Top             =   4800
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   295
      Left            =   4320
      TabIndex        =   295
      Top             =   6240
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   294
      Left            =   4800
      TabIndex        =   294
      Top             =   2520
      Width           =   615
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   293
      Left            =   7920
      TabIndex        =   293
      Top             =   2880
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   180
      Index           =   292
      Left            =   7320
      TabIndex        =   292
      Top             =   2280
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   291
      Left            =   7320
      TabIndex        =   291
      Top             =   2400
      Width           =   615
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   290
      Left            =   9120
      TabIndex        =   290
      Top             =   6600
      Width           =   135
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   289
      Left            =   9120
      TabIndex        =   289
      Top             =   6000
      Width           =   135
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   900
      Index           =   288
      Left            =   9240
      TabIndex        =   288
      Top             =   6000
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   540
      Index           =   287
      Left            =   9360
      TabIndex        =   287
      Top             =   5280
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   1140
      Index           =   286
      Left            =   9120
      TabIndex        =   286
      Top             =   5520
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   285
      Left            =   9840
      TabIndex        =   285
      Top             =   6600
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   284
      Left            =   9360
      TabIndex        =   284
      Top             =   5760
      Width           =   735
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   283
      Left            =   9840
      TabIndex        =   283
      Top             =   5520
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   660
      Index           =   282
      Left            =   10080
      TabIndex        =   282
      Top             =   4920
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   540
      Index           =   281
      Left            =   10320
      TabIndex        =   281
      Top             =   4680
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   660
      Index           =   280
      Left            =   10560
      TabIndex        =   280
      Top             =   3840
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   279
      Left            =   10560
      TabIndex        =   279
      Top             =   2760
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   780
      Index           =   278
      Left            =   10560
      TabIndex        =   278
      Top             =   4680
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   900
      Index           =   277
      Left            =   10320
      TabIndex        =   277
      Top             =   2520
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   540
      Index           =   276
      Left            =   10080
      TabIndex        =   276
      Top             =   2520
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   275
      Left            =   9840
      TabIndex        =   275
      Top             =   2760
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   274
      Left            =   9480
      TabIndex        =   274
      Top             =   2520
      Width           =   1095
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   273
      Left            =   8640
      TabIndex        =   273
      Top             =   3000
      Width           =   135
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   272
      Left            =   8640
      TabIndex        =   272
      Top             =   2760
      Width           =   495
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   271
      Left            =   9000
      TabIndex        =   271
      Top             =   3840
      Width           =   135
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   1140
      Index           =   270
      Left            =   9000
      TabIndex        =   270
      Top             =   2760
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   180
      Index           =   269
      Left            =   8280
      TabIndex        =   269
      Top             =   4560
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   180
      Index           =   268
      Left            =   7800
      TabIndex        =   268
      Top             =   4560
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   267
      Left            =   7800
      TabIndex        =   267
      Top             =   4680
      Width           =   495
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   266
      Left            =   7800
      TabIndex        =   266
      Top             =   4560
      Width           =   495
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   265
      Left            =   7320
      TabIndex        =   265
      Top             =   4440
      Width           =   135
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   264
      Left            =   7320
      TabIndex        =   264
      Top             =   4200
      Width           =   135
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   263
      Left            =   7320
      TabIndex        =   263
      Top             =   4200
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   262
      Left            =   7080
      TabIndex        =   262
      Top             =   3960
      Width           =   135
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   540
      Index           =   261
      Left            =   7080
      TabIndex        =   261
      Top             =   3960
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   260
      Left            =   6720
      TabIndex        =   260
      Top             =   3720
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   259
      Left            =   6120
      TabIndex        =   259
      Top             =   3720
      Width           =   375
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   258
      Left            =   6480
      TabIndex        =   258
      Top             =   3360
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   257
      Left            =   9360
      TabIndex        =   257
      Top             =   2760
      Width           =   495
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   256
      Left            =   5040
      TabIndex        =   256
      Top             =   3960
      Width           =   855
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   255
      Left            =   5040
      TabIndex        =   255
      Top             =   3840
      Width           =   855
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   660
      Index           =   254
      Left            =   5880
      TabIndex        =   254
      Top             =   3360
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   253
      Left            =   4320
      TabIndex        =   253
      Top             =   3720
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   180
      Index           =   252
      Left            =   4560
      TabIndex        =   252
      Top             =   3600
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   251
      Left            =   4560
      TabIndex        =   251
      Top             =   3600
      Width           =   1335
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   900
      Index           =   250
      Left            =   4560
      TabIndex        =   250
      Top             =   3960
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   249
      Left            =   6600
      TabIndex        =   249
      Top             =   5280
      Width           =   135
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   540
      Index           =   248
      Left            =   8640
      TabIndex        =   248
      Top             =   6120
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   247
      Left            =   8520
      TabIndex        =   247
      Top             =   6600
      Width           =   135
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   246
      Left            =   8640
      TabIndex        =   246
      Top             =   4080
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   245
      Left            =   8520
      TabIndex        =   245
      Top             =   6360
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   244
      Left            =   6000
      TabIndex        =   244
      Top             =   6600
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   243
      Left            =   8280
      TabIndex        =   243
      Top             =   6600
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   242
      Left            =   7080
      TabIndex        =   242
      Top             =   6600
      Width           =   1215
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   241
      Left            =   7080
      TabIndex        =   241
      Top             =   6360
      Width           =   1455
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   240
      Left            =   7080
      TabIndex        =   240
      Top             =   6000
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   660
      Index           =   239
      Left            =   7560
      TabIndex        =   239
      Top             =   5520
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   238
      Left            =   7080
      TabIndex        =   238
      Top             =   6120
      Width           =   1575
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   660
      Index           =   237
      Left            =   6840
      TabIndex        =   237
      Top             =   6000
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   236
      Left            =   6600
      TabIndex        =   236
      Top             =   5760
      Width           =   495
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   235
      Left            =   6360
      TabIndex        =   235
      Top             =   5760
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   660
      Index           =   234
      Left            =   6600
      TabIndex        =   234
      Top             =   5760
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   233
      Left            =   10560
      TabIndex        =   233
      Top             =   6360
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   232
      Left            =   10320
      TabIndex        =   232
      Top             =   5400
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   231
      Left            =   9840
      TabIndex        =   231
      Top             =   4680
      Width           =   495
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   180
      Index           =   230
      Left            =   9840
      TabIndex        =   230
      Top             =   4680
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   1020
      Index           =   229
      Left            =   10560
      TabIndex        =   229
      Top             =   5640
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   1020
      Index           =   228
      Left            =   10320
      TabIndex        =   228
      Top             =   5400
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   227
      Left            =   10080
      TabIndex        =   227
      Top             =   6600
      Width           =   495
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   226
      Left            =   9480
      TabIndex        =   226
      Top             =   6600
      Width           =   375
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   225
      Left            =   9480
      TabIndex        =   225
      Top             =   6000
      Width           =   375
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   660
      Index           =   224
      Left            =   10080
      TabIndex        =   224
      Top             =   6000
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   223
      Left            =   9720
      TabIndex        =   223
      Top             =   6360
      Width           =   375
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   222
      Left            =   9720
      TabIndex        =   222
      Top             =   6240
      Width           =   135
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   180
      Index           =   221
      Left            =   9840
      TabIndex        =   221
      Top             =   6240
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   220
      Left            =   7920
      TabIndex        =   220
      Top             =   3000
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   180
      Index           =   219
      Left            =   7920
      TabIndex        =   219
      Top             =   2880
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   1260
      Index           =   218
      Left            =   7440
      TabIndex        =   218
      Top             =   3000
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   1260
      Index           =   217
      Left            =   7200
      TabIndex        =   217
      Top             =   2760
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   216
      Left            =   7200
      TabIndex        =   216
      Top             =   2760
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   215
      Left            =   7440
      TabIndex        =   215
      Top             =   3000
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   214
      Left            =   5520
      TabIndex        =   214
      Top             =   6360
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   213
      Left            =   5520
      TabIndex        =   213
      Top             =   6600
      Width           =   1335
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   212
      Left            =   7680
      TabIndex        =   212
      Top             =   2640
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   180
      Index           =   211
      Left            =   7440
      TabIndex        =   211
      Top             =   2640
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   210
      Left            =   7680
      TabIndex        =   210
      Top             =   2640
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   209
      Left            =   6600
      TabIndex        =   209
      Top             =   2520
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   180
      Index           =   208
      Left            =   6600
      TabIndex        =   208
      Top             =   2400
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   207
      Left            =   6840
      TabIndex        =   207
      Top             =   2280
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   180
      Index           =   206
      Left            =   5880
      TabIndex        =   206
      Top             =   2280
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   205
      Left            =   5880
      TabIndex        =   205
      Top             =   2400
      Width           =   735
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   204
      Left            =   8160
      TabIndex        =   204
      Top             =   3600
      Width           =   135
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   203
      Left            =   3360
      TabIndex        =   203
      Top             =   4560
      Width           =   975
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   202
      Left            =   3360
      TabIndex        =   202
      Top             =   4440
      Width           =   975
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   37
      Left            =   5040
      TabIndex        =   201
      Top             =   3840
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   201
      Left            =   8880
      TabIndex        =   200
      Top             =   4560
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   200
      Left            =   8760
      TabIndex        =   199
      Top             =   4560
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   199
      Left            =   7680
      TabIndex        =   198
      Top             =   4320
      Width           =   735
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   198
      Left            =   8640
      TabIndex        =   197
      Top             =   4320
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   197
      Left            =   8880
      TabIndex        =   196
      Top             =   4080
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   196
      Left            =   7680
      TabIndex        =   195
      Top             =   4080
      Width           =   735
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   900
      Index           =   195
      Left            =   7680
      TabIndex        =   194
      Top             =   3240
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   194
      Left            =   8520
      TabIndex        =   193
      Top             =   3240
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   193
      Left            =   7680
      TabIndex        =   192
      Top             =   3240
      Width           =   855
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   180
      Index           =   192
      Left            =   5640
      TabIndex        =   191
      Top             =   5520
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   191
      Left            =   5280
      TabIndex        =   190
      Top             =   5520
      Width           =   375
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   190
      Left            =   5280
      TabIndex        =   189
      Top             =   4920
      Width           =   135
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   180
      Index           =   189
      Left            =   5640
      TabIndex        =   188
      Top             =   4680
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   188
      Left            =   6960
      TabIndex        =   187
      Top             =   5160
      Width           =   135
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   187
      Left            =   6960
      TabIndex        =   186
      Top             =   5280
      Width           =   135
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   180
      Index           =   186
      Left            =   6960
      TabIndex        =   185
      Top             =   5160
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   185
      Left            =   7080
      TabIndex        =   184
      Top             =   5160
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   184
      Left            =   6360
      TabIndex        =   183
      Top             =   5520
      Width           =   735
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   183
      Left            =   6480
      TabIndex        =   182
      Top             =   4920
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   182
      Left            =   6360
      TabIndex        =   181
      Top             =   5280
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   180
      Index           =   181
      Left            =   6480
      TabIndex        =   180
      Top             =   4920
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   180
      Left            =   5520
      TabIndex        =   179
      Top             =   5280
      Width           =   375
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   179
      Left            =   5520
      TabIndex        =   178
      Top             =   5040
      Width           =   375
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   178
      Left            =   6120
      TabIndex        =   177
      Top             =   5280
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   177
      Left            =   6120
      TabIndex        =   176
      Top             =   5040
      Width           =   375
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   176
      Left            =   5520
      TabIndex        =   175
      Top             =   5040
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   175
      Left            =   6720
      TabIndex        =   174
      Top             =   4920
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   174
      Left            =   6120
      TabIndex        =   173
      Top             =   5280
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   173
      Left            =   5880
      TabIndex        =   172
      Top             =   5280
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   172
      Left            =   6120
      TabIndex        =   171
      Top             =   4680
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   171
      Left            =   5880
      TabIndex        =   170
      Top             =   5640
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   780
      Index           =   170
      Left            =   5280
      TabIndex        =   169
      Top             =   4920
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   1140
      Index           =   169
      Left            =   8880
      TabIndex        =   168
      Top             =   5760
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   180
      Index           =   168
      Left            =   9720
      TabIndex        =   167
      Top             =   6240
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   660
      Index           =   167
      Left            =   9480
      TabIndex        =   166
      Top             =   6000
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   900
      Index           =   166
      Left            =   3360
      TabIndex        =   165
      Top             =   5520
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   660
      Index           =   165
      Left            =   3120
      TabIndex        =   164
      Top             =   5760
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   164
      Left            =   5880
      TabIndex        =   163
      Top             =   4680
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   163
      Left            =   5280
      TabIndex        =   162
      Top             =   5640
      Width           =   375
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   162
      Left            =   7920
      TabIndex        =   161
      Top             =   3480
      Width           =   375
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   161
      Left            =   5880
      TabIndex        =   160
      Top             =   3120
      Width           =   615
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   160
      Left            =   5040
      TabIndex        =   159
      Top             =   4680
      Width           =   615
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   159
      Left            =   3120
      TabIndex        =   158
      Top             =   5520
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   158
      Left            =   5880
      TabIndex        =   157
      Top             =   3360
      Width           =   615
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   157
      Left            =   5520
      TabIndex        =   156
      Top             =   6360
      Width           =   1095
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   156
      Left            =   5040
      TabIndex        =   155
      Top             =   6600
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   540
      Index           =   155
      Left            =   5280
      TabIndex        =   154
      Top             =   6120
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   154
      Left            =   5280
      TabIndex        =   153
      Top             =   6120
      Width           =   1095
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   153
      Left            =   5040
      TabIndex        =   152
      Top             =   5880
      Width           =   1095
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   780
      Index           =   152
      Left            =   5040
      TabIndex        =   151
      Top             =   5880
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   151
      Left            =   4320
      TabIndex        =   150
      Top             =   4200
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   180
      Index           =   150
      Left            =   8280
      TabIndex        =   149
      Top             =   3480
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   149
      Left            =   4560
      TabIndex        =   148
      Top             =   6480
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   180
      Index           =   148
      Left            =   3600
      TabIndex        =   147
      Top             =   6480
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   147
      Left            =   5880
      TabIndex        =   146
      Top             =   4680
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   146
      Left            =   4320
      TabIndex        =   145
      Top             =   4800
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   145
      Left            =   4800
      TabIndex        =   144
      Top             =   5040
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   144
      Left            =   4320
      TabIndex        =   143
      Top             =   5040
      Width           =   495
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   143
      Left            =   6840
      TabIndex        =   142
      Top             =   4680
      Width           =   735
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   142
      Left            =   9360
      TabIndex        =   141
      Top             =   4440
      Width           =   1215
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   141
      Left            =   3000
      TabIndex        =   140
      Top             =   6600
      Width           =   615
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   140
      Left            =   3600
      TabIndex        =   139
      Top             =   6480
      Width           =   975
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   139
      Left            =   3360
      TabIndex        =   138
      Top             =   4800
      Width           =   975
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   138
      Left            =   4320
      TabIndex        =   137
      Top             =   3360
      Width           =   855
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   900
      Index           =   137
      Left            =   3360
      TabIndex        =   136
      Top             =   4440
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   136
      Left            =   3120
      TabIndex        =   135
      Top             =   5280
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   135
      Left            =   2640
      TabIndex        =   134
      Top             =   4440
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   134
      Left            =   2640
      TabIndex        =   133
      Top             =   4440
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   900
      Index           =   133
      Left            =   3120
      TabIndex        =   132
      Top             =   4200
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   660
      Index           =   132
      Left            =   10320
      TabIndex        =   131
      Top             =   3600
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   660
      Index           =   131
      Left            =   10560
      TabIndex        =   130
      Top             =   3000
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   130
      Left            =   9840
      TabIndex        =   129
      Top             =   4200
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   129
      Left            =   9120
      TabIndex        =   128
      Top             =   3960
      Width           =   735
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   1020
      Index           =   128
      Left            =   10080
      TabIndex        =   127
      Top             =   3240
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   780
      Index           =   127
      Left            =   9600
      TabIndex        =   126
      Top             =   3000
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   1500
      Index           =   126
      Left            =   9120
      TabIndex        =   125
      Top             =   2760
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   125
      Left            =   9120
      TabIndex        =   124
      Top             =   4200
      Width           =   495
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   124
      Left            =   9360
      TabIndex        =   123
      Top             =   3720
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   540
      Index           =   123
      Left            =   9840
      TabIndex        =   122
      Top             =   3480
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   122
      Left            =   9600
      TabIndex        =   121
      Top             =   3240
      Width           =   495
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   780
      Index           =   121
      Left            =   9360
      TabIndex        =   120
      Top             =   2760
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   120
      Left            =   10080
      TabIndex        =   119
      Top             =   3600
      Width           =   495
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   119
      Left            =   3960
      TabIndex        =   118
      Top             =   3960
      Width           =   615
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   118
      Left            =   3960
      TabIndex        =   117
      Top             =   3720
      Width           =   135
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   660
      Index           =   117
      Left            =   4080
      TabIndex        =   116
      Top             =   3360
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   116
      Left            =   4320
      TabIndex        =   115
      Top             =   3360
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   180
      Index           =   115
      Left            =   8400
      TabIndex        =   114
      Top             =   5520
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   114
      Left            =   8040
      TabIndex        =   113
      Top             =   5520
      Width           =   375
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   113
      Left            =   8040
      TabIndex        =   112
      Top             =   5640
      Width           =   375
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   112
      Left            =   8640
      TabIndex        =   111
      Top             =   5520
      Width           =   495
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   540
      Index           =   111
      Left            =   8040
      TabIndex        =   110
      Top             =   5160
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   110
      Left            =   7800
      TabIndex        =   109
      Top             =   5520
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   109
      Left            =   8640
      TabIndex        =   108
      Top             =   5520
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   108
      Left            =   7800
      TabIndex        =   107
      Top             =   5880
      Width           =   855
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   107
      Left            =   7920
      TabIndex        =   106
      Top             =   3480
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   900
      Index           =   106
      Left            =   8760
      TabIndex        =   105
      Top             =   3000
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   105
      Left            =   7920
      TabIndex        =   104
      Top             =   3840
      Width           =   855
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   900
      Index           =   104
      Left            =   6960
      TabIndex        =   103
      Top             =   2880
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   103
      Left            =   3960
      TabIndex        =   102
      Top             =   3720
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   102
      Left            =   2640
      TabIndex        =   101
      Top             =   4200
      Width           =   1455
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   101
      Left            =   8760
      TabIndex        =   100
      Top             =   4800
      Width           =   375
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   660
      Index           =   100
      Left            =   9120
      TabIndex        =   99
      Top             =   4200
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   99
      Left            =   8520
      TabIndex        =   98
      Top             =   4560
      Width           =   375
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   98
      Left            =   8280
      TabIndex        =   97
      Top             =   5280
      Width           =   1335
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   97
      Left            =   7560
      TabIndex        =   96
      Top             =   4680
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   96
      Left            =   6960
      TabIndex        =   95
      Top             =   4920
      Width           =   375
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   1020
      Index           =   95
      Left            =   7320
      TabIndex        =   94
      Top             =   4920
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   94
      Left            =   7320
      TabIndex        =   93
      Top             =   5280
      Width           =   495
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   93
      Left            =   7800
      TabIndex        =   92
      Top             =   4920
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   92
      Left            =   7800
      TabIndex        =   91
      Top             =   4920
      Width           =   495
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   91
      Left            =   8280
      TabIndex        =   90
      Top             =   4920
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   540
      Index           =   90
      Left            =   9600
      TabIndex        =   89
      Top             =   5280
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   540
      Index           =   89
      Left            =   8520
      TabIndex        =   88
      Top             =   4560
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   88
      Left            =   9360
      TabIndex        =   87
      Top             =   4680
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   660
      Index           =   87
      Left            =   9600
      TabIndex        =   86
      Top             =   4440
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   540
      Index           =   86
      Left            =   9840
      TabIndex        =   85
      Top             =   5040
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   85
      Left            =   8520
      TabIndex        =   84
      Top             =   5040
      Width           =   855
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   84
      Left            =   9600
      TabIndex        =   83
      Top             =   5040
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   83
      Left            =   4560
      TabIndex        =   82
      Top             =   5640
      Width           =   495
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   780
      Index           =   81
      Left            =   3840
      TabIndex        =   81
      Top             =   4800
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   82
      Left            =   4080
      TabIndex        =   80
      Top             =   5280
      Width           =   495
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   540
      Index           =   80
      Left            =   4320
      TabIndex        =   79
      Top             =   5520
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   79
      Left            =   3840
      TabIndex        =   78
      Top             =   5760
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   78
      Left            =   3600
      TabIndex        =   77
      Top             =   6240
      Width           =   495
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   1260
      Index           =   77
      Left            =   3600
      TabIndex        =   76
      Top             =   5040
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   780
      Index           =   76
      Left            =   4080
      TabIndex        =   75
      Top             =   5040
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   1020
      Index           =   75
      Left            =   4560
      TabIndex        =   74
      Top             =   5280
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   74
      Left            =   3600
      TabIndex        =   73
      Top             =   6000
      Width           =   735
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   73
      Left            =   4320
      TabIndex        =   72
      Top             =   6240
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   1020
      Index           =   72
      Left            =   5040
      TabIndex        =   71
      Top             =   4680
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   1020
      Index           =   71
      Left            =   4800
      TabIndex        =   70
      Top             =   5640
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   780
      Index           =   70
      Left            =   6840
      TabIndex        =   69
      Top             =   3960
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   69
      Left            =   6120
      TabIndex        =   68
      Top             =   3960
      Width           =   735
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   68
      Left            =   6120
      TabIndex        =   67
      Top             =   3600
      Width           =   375
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   67
      Left            =   6120
      TabIndex        =   66
      Top             =   3600
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   66
      Left            =   6360
      TabIndex        =   65
      Top             =   4440
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   65
      Left            =   5040
      TabIndex        =   64
      Top             =   4200
      Width           =   1575
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   1260
      Index           =   64
      Left            =   4800
      TabIndex        =   63
      Top             =   3840
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   63
      Left            =   4800
      TabIndex        =   62
      Top             =   4440
      Width           =   1575
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   540
      Index           =   62
      Left            =   6600
      TabIndex        =   61
      Top             =   4200
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   61
      Left            =   6360
      TabIndex        =   60
      Top             =   4680
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   900
      Index           =   60
      Left            =   6720
      TabIndex        =   59
      Top             =   2880
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   540
      Index           =   59
      Left            =   8160
      TabIndex        =   58
      Top             =   2520
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   58
      Left            =   9240
      TabIndex        =   57
      Top             =   2280
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   780
      Index           =   57
      Left            =   8400
      TabIndex        =   56
      Top             =   2520
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   56
      Left            =   8400
      TabIndex        =   55
      Top             =   2520
      Width           =   855
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   55
      Left            =   7080
      TabIndex        =   54
      Top             =   2640
      Width           =   375
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   54
      Left            =   7920
      TabIndex        =   53
      Top             =   2400
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   53
      Left            =   6120
      TabIndex        =   52
      Top             =   2880
      Width           =   615
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   52
      Left            =   6120
      TabIndex        =   51
      Top             =   2640
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   51
      Left            =   5640
      TabIndex        =   50
      Top             =   2640
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   50
      Left            =   4560
      TabIndex        =   49
      Top             =   2640
      Width           =   855
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   49
      Left            =   7080
      TabIndex        =   48
      Top             =   2280
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   48
      Left            =   6120
      TabIndex        =   47
      Top             =   2640
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   540
      Index           =   47
      Left            =   5880
      TabIndex        =   46
      Top             =   2640
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   46
      Left            =   5640
      TabIndex        =   45
      Top             =   2280
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   180
      Index           =   45
      Left            =   5400
      TabIndex        =   44
      Top             =   2520
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   44
      Left            =   4560
      TabIndex        =   43
      Top             =   2280
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   43
      Left            =   5400
      TabIndex        =   42
      Top             =   3360
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   42
      Left            =   5400
      TabIndex        =   41
      Top             =   3120
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   780
      Index           =   41
      Left            =   5640
      TabIndex        =   40
      Top             =   2880
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   40
      Left            =   4320
      TabIndex        =   39
      Top             =   2520
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   39
      Left            =   4080
      TabIndex        =   38
      Top             =   2520
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   38
      Left            =   3840
      TabIndex        =   37
      Top             =   3120
      Width           =   1575
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   660
      Index           =   36
      Left            =   3840
      TabIndex        =   36
      Top             =   2880
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   35
      Left            =   4080
      TabIndex        =   35
      Top             =   2520
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   34
      Left            =   3600
      TabIndex        =   34
      Top             =   2880
      Width           =   495
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   33
      Left            =   3360
      TabIndex        =   33
      Top             =   3480
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   32
      Left            =   2640
      TabIndex        =   32
      Top             =   3240
      Width           =   735
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   1020
      Index           =   31
      Left            =   3600
      TabIndex        =   31
      Top             =   2520
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   30
      Left            =   3360
      TabIndex        =   30
      Top             =   2520
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   29
      Left            =   2400
      TabIndex        =   29
      Top             =   6600
      Width           =   375
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   28
      Left            =   2640
      TabIndex        =   28
      Top             =   6360
      Width           =   495
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   900
      Index           =   26
      Left            =   2640
      TabIndex        =   27
      Top             =   5520
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   1140
      Index           =   25
      Left            =   2400
      TabIndex        =   26
      Top             =   3960
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   24
      Left            =   2400
      TabIndex        =   25
      Top             =   3720
      Width           =   1335
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   780
      Index           =   23
      Left            =   3120
      TabIndex        =   24
      Top             =   2280
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   540
      Index           =   22
      Left            =   2880
      TabIndex        =   23
      Top             =   2280
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   1260
      Index           =   21
      Left            =   2640
      TabIndex        =   22
      Top             =   2280
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   660
      Index           =   20
      Left            =   2400
      TabIndex        =   21
      Top             =   3120
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   18
      Left            =   2400
      TabIndex        =   20
      Top             =   5520
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   17
      Left            =   2160
      TabIndex        =   19
      Top             =   5280
      Width           =   495
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   16
      Left            =   2400
      TabIndex        =   18
      Top             =   5040
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   15
      Left            =   3360
      TabIndex        =   17
      Top             =   3960
      Width           =   615
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   14
      Left            =   2160
      TabIndex        =   16
      Top             =   3960
      Width           =   975
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   13
      Left            =   2640
      TabIndex        =   15
      Top             =   3480
      Width           =   495
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   12
      Left            =   2880
      TabIndex        =   14
      Top             =   3000
      Width           =   255
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   420
      Index           =   11
      Left            =   3840
      TabIndex        =   13
      Top             =   2280
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   10
      Left            =   2760
      TabIndex        =   12
      Top             =   6600
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   1500
      Index           =   9
      Left            =   2880
      TabIndex        =   11
      Top             =   4680
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   1140
      Index           =   8
      Left            =   2400
      TabIndex        =   10
      Top             =   5520
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   7
      Left            =   2640
      TabIndex        =   9
      Top             =   5040
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   300
      Index           =   6
      Left            =   3720
      TabIndex        =   8
      Top             =   3720
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   540
      Index           =   5
      Left            =   3360
      TabIndex        =   7
      Top             =   2760
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   4
      Left            =   4320
      TabIndex        =   6
      Top             =   2880
      Width           =   1335
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   4380
      Index           =   3
      Left            =   10800
      TabIndex        =   5
      Top             =   2280
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   3780
      Index           =   2
      Left            =   2160
      TabIndex        =   4
      Top             =   3120
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   1
      Left            =   2160
      TabIndex        =   3
      Top             =   6840
      Width           =   8775
   End
   Begin VB.Label Linha 
      BackColor       =   &H80000007&
      Height          =   60
      Index           =   0
      Left            =   1320
      TabIndex        =   2
      Top             =   2280
      Width           =   9495
   End
   Begin VB.Label lbl_descricao 
      BackColor       =   &H80000005&
      Caption         =   "Passe pelo labirinto como �ltima etapa para adquirir seu diploma"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   120
      TabIndex        =   1
      Top             =   720
      Width           =   16695
   End
   Begin VB.Label lbl_titulo 
      BackColor       =   &H80000005&
      Caption         =   "Defesa da disserta��o"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   26.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   735
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   9975
   End
   Begin VB.Label Linha 
      BackColor       =   &H00000000&
      Height          =   300
      Index           =   302
      Left            =   9360
      TabIndex        =   305
      Top             =   5040
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H00000000&
      Height          =   300
      Index           =   303
      Left            =   9120
      TabIndex        =   306
      Top             =   2520
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H00000000&
      Height          =   300
      Index           =   304
      Left            =   7320
      TabIndex        =   307
      Top             =   4680
      Width           =   60
   End
   Begin VB.Label Linha 
      BackColor       =   &H00000000&
      Height          =   300
      Index           =   305
      Left            =   7080
      TabIndex        =   308
      Top             =   4440
      Width           =   60
   End
End
Attribute VB_Name = "frm09_defesa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Ativo As String

Private Sub Inicializa()
  Ativo = "N"
  Me.Tempo.Interval = 0
  Me.Inicio.Caption = "Iniciar"
  Me.Linha(302).Visible = False
  Me.Linha(303).Visible = False
  Me.Linha(304).Visible = False
  Me.Linha(305).Visible = False
End Sub

Private Sub Command1_Click()
  MsgBox "Fase " & Fase & " conclu�da!", vbInformation
  MudaFase Fase
End Sub

Private Sub Form_Load()
  Inicializa
End Sub

Private Sub Form_Unload(Cancel As Integer)
  End
End Sub

Private Sub img_diploma_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Ativo = "S" Then
    Ativo = "N"
    MsgBox "Fase " & Fase & " conclu�da!", vbInformation
    MudaFase Fase
  End If
End Sub

Private Sub Inicio_Click()
  Ativo = "S"
  Me.Inicio.Caption = "0"
  Me.Tempo.Interval = 1000
End Sub

Private Sub Linha_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Ativo = "S" Then
    MsgBox "Errou. Clique em iniciar para recome�ar", vbCritical
    Inicializa
  End If
End Sub

Private Sub Tempo_Timer()
  If Ativo = "S" Then
    Me.Inicio.Caption = CInt(Me.Inicio.Caption) + 1
    If CInt(Me.Inicio.Caption) Mod 30 = 0 Then
      Me.Linha(305).Visible = True
      Me.Linha(303).Visible = True
      Me.Linha(302).Visible = False
      Me.Linha(304).Visible = False
    ElseIf CInt(Me.Inicio.Caption) Mod 15 = 0 Then
      Me.Linha(305).Visible = False
      Me.Linha(303).Visible = False
      Me.Linha(302).Visible = True
      Me.Linha(304).Visible = True
    End If
  End If
End Sub
